<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Update Category</li>
                    </ol>
                </div>
            </div>            

            <span id="msg"></span>
            
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">                     
                        <div class="card-body">
                            <input type="hidden" class="form-control" id="id" name="id" value="${category.id}">
                            
                            <div class="form-group">
                                <label>CATEGORY NAME:</label>

                                <input type="text" class="form-control" id="name" name="name" value="${category.name}" placeholder="CATEGORY NAME" autofocus>
                            </div> 

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group">
                                        <label>SUB CATEGORY NAME:</label>

                                        <input type="text" class="form-control" id="sub_name" name="sub_name" placeholder="SUB CATEGORY NAME">
                                    </div> 
                                </div>

                                <div class="col-md-1" style="margin-top: 31px;">
                                    <button id="addTableBtn" class="btn btn-primary">Add</button>
                                </div>
                            </div>                          

                            <table id="table" class="table table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>SUB CATEGORY</th>
                                        <th style="width: 5px;"></th>
                                    </tr>   
                                </thead>

                                <tbody>
                                    <c:forEach items="${category.subCategories}" var="object">
                                        <tr>
                                            <td>${object.name}</td>
                                            <td><button class="btn btn-danger red" onclick="deleteRow(this)" type="button">Delete</button></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>

                                <tfoot>
                                </tfoot>			
                            </table>   
                        </div>

                        <div class="card-footer">
                            <button id="updateBtn" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $(".category").removeClass("treeview").addClass("treeview menu-open");
        $("#category-menu").css('display', 'block');
        $("#view_category").addClass("active");

        $("#addTableBtn").click(function () {
            var subCategory = $("#sub_name").val();

            var tbody = "<tr><td>" + subCategory + "</td><td><button class=\"btn btn-danger red\" onclick=\"deleteRow(this)\" type=\"button\">Delete</button></td></tr>";

            $("#table").find("tbody").append(tbody);

            $("#sub_name").val("");
            $("#sub_name").focus();
        });

        $("#updateBtn").click(function () {
            var id = $("#id").val();
            var name = $("#name").val();

            var items = [];

            $('#table tbody tr').each(function () {
                var name = $(this).find("td").eq(0).text();
                items.push(name);
            });

            $.ajax({
                type: "POST",
                url: "/update_category",
                data: {
                    id: id,
                    name: name,
                    items: items
                },
                success: function (res) {
                    if (res == "200") {
                        $('#msg').append('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><h4><i class="icon fa fa-check"></i> Category updated successfully</h4></div>'); 
                    } else {
                        $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><h4><i class="icon fa fa-check"></i> Category updated failed</h4></div>');
                    }
                },
                error: function (result) {
                    $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button><h4><i class="icon fa fa-check"></i> Category updated failed</h4></div>');
                }
            });
        });
    });

    function deleteRow(e) {
        var a = e.parentNode.parentNode;
        a.parentNode.removeChild(a);
    }
</script>
</body>
</html>
