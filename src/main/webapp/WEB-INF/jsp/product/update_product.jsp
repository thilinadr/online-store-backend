<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Update Product</li>
                    </ol>
                </div>
            </div>

            <c:if test="${not empty msg}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${msg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty warnmsg}">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty errmsg}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

                </div>
            </c:if>

            <span id="msg"></span>

        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">   
                        <form id="form" action="" method="post"> 
                            <div class="card-body">
                                <input type="hidden" class="form-control" id="id" name="id" value="${product.id}">

                                <div class="form-group">
                                    <label>CATEGORY:</label>

                                    <select class="form-control select2" id="category" name="category" data-placeholder="Select a Category" style="width: 100%;">

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>SUB CATEGORY:</label>

                                    <select class="form-control select2" id="sub_category" name="sub_category" data-placeholder="Select a Sub Category" style="width: 100%;">

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>PRODUCT NAME:</label>

                                    <input type="text" class="form-control" id="name" name="name" value="${product.name}" placeholder="PRODUCT NAME" autofocus>
                                </div> 

                                <div class="form-group">
                                    <label>DESCRIPTION:</label>

                                    <input type="textarea" class="form-control" id="description" name="description" value="${product.description}" placeholder="DESCRIPTION">
                                </div> 

                                <div class="form-group">
                                    <label>PRODUCT PRICE:</label>

                                    <input type="text" class="form-control" id="price" name="price" value="${product.price}" placeholder="PRODUCT PRICE">
                                </div> 

                                <div class="form-group">
                                    <label>IMAGE:</label>

                                    <input type="file" class="form-control" id="image" name="image" multiple>
                                                                        
                                    <c:forEach items="${product.images}" var="img">
                                        <span class="pip">
                                            <img class="imageThumb" src="../uploads/${img.name}"/>
                                            <br/><!--<span class="mdi mdi-close remove">Remove</span>-->
                                        </span>
                                    </c:forEach>
                                </div> 

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>PRODUCT SIZE:</label>

                                            <input type="text" class="form-control" id="size" name="size" placeholder="PRODUCT SIZE">
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>PRODUCT QTY:</label>

                                            <input type="number" class="form-control" id="qty" name="qty" placeholder="PRODUCT QTY">
                                        </div>
                                    </div>

                                    <div class="col-md-1" style="margin-top: 31px;">
                                        <button id="addTableBtn" class="btn btn-primary">Add</button>
                                    </div>
                                </div>                          

                                <table id="table" class="table table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="display: none;">ID</th>
                                            <th>SIZE</th>
                                            <th>QTY</th>
                                            <th style="width: 5px;"></th>
                                        </tr>   
                                    </thead>

                                    <tbody>
                                        <c:forEach items="${product.productsizes}" var="object">
                                            <tr>
                                                <td style="display: none;">${object.id}</td>
                                                <td>${object.name}</td>
                                                <td>${object.qty}</td>
                                                <td><button class="btn btn-danger red" onclick="deleteRow(this)" type="button">Delete</button></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>

                                    <tfoot>
                                    </tfoot>			
                                </table>   
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="updateBtn" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $(".product").removeClass("treeview").addClass("treeview menu-open");
        $("#product-menu").css('display', 'block');
        $("#view_product").addClass("active");
        
        var categoryId = '${product.categoryId}';
        var subCategoryId = '${product.subCategoryId}';

        $.ajax({
            type: "GET",
            url: "../category_dt",
            success: function (category) {
                $("#category").append('<option value="">Select a category</option>');

                $.each(category, function (i, o) {
                    if(o.id == categoryId){
                        $("#category").append('<option value="' + o.id + '" selected>' + o.name + '</option>');
                    }else{
                        $("#category").append('<option value="' + o.id + '">' + o.name + '</option>');
                    }               
                });
            },
            error: function (result) {
            }
        });
        
        if(subCategoryId != ""){
            $.ajax({
                type: "GET",
                url: "../sub_category_dt/" + categoryId,
                success: function (category) {
                    $("#sub_category").append('<option value="">Select a sub category</option>');

                    $.each(category, function (i, o) {
                        if(o.id == subCategoryId){
                            $("#sub_category").append('<option value="' + o.id + '" selected>' + o.name + '</option>');
                        }else{
                            $("#sub_category").append('<option value="' + o.id + '">' + o.name + '</option>');
                        }    
                    });
                },
                error: function (result) {
                }
            });
        }

        $('.select2').select2();

        $('#category').on('change', function () {
            var id = this.value;

            $.ajax({
                type: "GET",
                url: "../sub_category_dt/" + id,
                success: function (category) {
                    console.log(category);
                    $("#sub_category").append('<option value="">Select a sub category</option>');

                    $.each(category, function (i, o) {
                        if(o.id == subCategoryId){
                            $("#sub_category").append('<option value="' + o.id + '" selected>' + o.name + '</option>');
                        }else{
                            $("#sub_category").append('<option value="' + o.id + '">' + o.name + '</option>');
                        }    
                    });
                },
                error: function (result) {
                }
            });
        });

        $("#image").on("change", function (e) {
            var files = e.target.files,
                    filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i];
                var fileReader = new FileReader();
                fileReader.onload = (function (e) {
                    var file = e.target;
                    $("<span class=\"pip\">" +
                            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" data-file='" + file.name + "' title=\"" + file.name + "\"/>" +
                            "<br/><span class=\"mdi mdi-close remove\">Remove</span>" +
                            "</span>").insertAfter("#image");
                    $(".remove").click(function () {
                        $(this).parent(".pip").remove();

                    });

                });
                fileReader.readAsDataURL(f);
            }
        });
        
        $(".remove").click(function () {
            $(this).parent(".pip").remove();
        });

        $("#addTableBtn").click(function (e) {
            e.preventDefault();

            var size = $("#size").val();
            var qty = $("#qty").val();

            var tbody = "<tr><td style=\"display:none;\"></td><td>" + size + "</td><td>" + qty + "</td><td><button class=\"btn btn-danger red\" onclick=\"deleteRow(this)\" type=\"button\">Delete</button></td></tr>";

            $("#table").find("tbody").append(tbody);

            $("#size").val("");
            $("#qty").val("");
            $("#size").focus();
        });

        $('#form').validate({
            rules: {
                sub_category: {
                    required: true
                },
                name: {
                    required: true
                },
                description: {
                    required: true
                },
                price: {
                    required: true
                }
            },
            submitHandler: function (form, event) {
                event.preventDefault();

                var length = $('#table >tbody >tr').length;

                if (length === 0) {
                    Swal.fire(
                            'Product size table is empty',
                            '',
                            'error'
                            );
                } else {
                    var items = [];

                    $('#table tbody tr').each(function () {
                        var id = $(this).find("td").eq(0).text();
                        var size = $(this).find("td").eq(1).text();
                        var qty = $(this).find("td").eq(2).text();

                        var item = {'id': id, 'name': size, 'qty': parseInt(qty)};

                        items.push(item);
                    });

                    var form = $('#form')[0];

                    var data = new FormData(form);
                    data.append('items', JSON.stringify(items));
                    data.delete("size");
                    data.delete("qty");

                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "../update_product",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            if (result == "200") {
                                $('#msg').append('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i>Product updated successfully</h4></div>');
                            } else {
                                $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i>Product update failed</h4></div>');
                            }
                        },
                        error: function (result) {
                            $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i>Product name already exists</h4></div>');
                        }
                    });
                }
            }
        });
    });

    function deleteRow(e) {
        var a = e.parentNode.parentNode;
        a.parentNode.removeChild(a);
    }
</script>
</body>
</html>
