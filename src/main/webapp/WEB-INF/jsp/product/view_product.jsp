<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>View Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">View Product</li>
                    </ol>
                </div>
            </div>

            <c:if test="${not empty msg}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${msg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty warnmsg}">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty errmsg}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

                </div>
            </c:if>

            <span id="msg"></span>

        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">     
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" class="table table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SUB CATEGORY</th>
                                            <th>PRODUCT NAME</th>
                                            <th>PRICE</th>
                                            <th>RATING</th>
                                            <th style="width: 5px;"></th>
                                            <th style="width: 5px;"></th>
                                            <th style="width: 5px;"></th>
                                        </tr>   
                                    </thead>

                                    <tbody>
                                    </tbody>

                                    <tfoot>
                                    </tfoot>			
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="offerModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Product Offer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="card card-secondary">   
                            <form id="form" action="" method="post"> 
                                <div class="card-body">
                                    <input type="hidden" class="form-control" id="product_id" name="product_id">
                                    <input type="hidden" class="form-control" id="id" name="id">
                                    
                                    <div class="form-group">
                                        <label>OFFER NAME:</label>

                                        <input type="text" class="form-control" id="name" name="name" placeholder="OFFER NAME" autofocus>
                                    </div> 

                                    <div class="form-group">
                                        <label>PERCENTAGE:</label>

                                        <input type="number" class="form-control" id="percentage" name="percentage" placeholder="PERCENTAGE">
                                    </div> 

                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="availability" name="availability">
                                        <label class="form-check-label">AVAILABILITY</label>
                                    </div>

                                </div>

                                <div class="card-footer">
                                    <button type="submit" id="saveBtn" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $(".product").removeClass("treeview").addClass("treeview menu-open");
        $("#product-menu").css('display', 'block');
        $("#view_product").addClass("active");

        var table = $('#table').DataTable({
            "pageLength": 10,
            "ajax": "/view_product_dt"
        });

        $('#form').validate({
            rules: {
                name: {
                    required: true
                },
                percentage: {
                    required: true
                }
            },
            submitHandler: function (form, event) {
                event.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "/add_offer",
                    data: $("#form").serialize(),
                    success: function (res) {
                        $('#form')[0].reset();
                        $('#offerModal').modal('hide');
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            }
        });

        $('#table tbody').on('click', 'td', function () {
            var col = $(this).parent().children().index($(this));
            var row = $(this).parent().parent().children().index($(this).parent());
            
            if(col === 4){
                var id = $('#table tbody tr:eq(' + row + ')').find("td a").eq(2).data("id");
                $("#product_id").val(id);
                
                $.ajax({
                    type: "GET",
                    url: "/product_offer/"+id,
                    success: function (res) {
                        if(res != ""){
                            $("#id").val(res.id);
                            $("#name").val(res.name);
                            $("#percentage").val(res.percentage);
                            
                            if(res.availability){
                               $("#availability").prop("checked", true); 
                            }else{
                               $("#availability").prop("checked", false); 
                            }
                        }else{
                            $("#id").val("");
                            $("#name").val("");
                            $("#percentage").val("");
                            $("#availability").prop("checked", false); 
                         }
                        
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            }

            if (col === 6) {
                var id = $('#table tbody tr:eq(' + row + ')').find("td a").eq(2).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                }).then((result) => {
                    if (result.value) {
                        deleteProduct(id);
                    }
                });
            }

        });
    });

    function deleteProduct(id) {
        $.ajax({
            type: "POST",
            url: "/delete_product",
            data: {
                id: id
            },
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                Swal.fire({
                    type: 'error',
                    title: 'Product delete failed...',
                    text: ''
                });
            }
        });
    }
</script>
</body>
</html>
