<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>View Banner</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">View Banner</li>
                    </ol>
                </div>
            </div>

            <c:if test="${not empty msg}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${msg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty warnmsg}">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty errmsg}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

                </div>
            </c:if>

            <span id="msg"></span>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">     
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" class="table table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>IMAGE</th>
                                            <th>DESCRIPTION</th>
                                            <th>URL</th>
                                            <th style="width: 5px;"></th>
                                        </tr>   
                                    </thead>

                                    <tbody>
                                    </tbody>

                                    <tfoot>
                                    </tfoot>			
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $(".administration").removeClass("treeview").addClass("treeview menu-open");
        $("#administration-menu").css('display', 'block');
        $("#banner").removeClass("has-treeview").addClass("has-treeview menu-open");
        $("#banner-menu").css('display', 'block');
        $("#view_banner").addClass("active");
        
        var table = $('#table').DataTable({
            "pageLength": 10,
            "ajax": "/view_banner_dt"
        });
        
        $('#table tbody').on('click', 'td', function () {
            var col = $(this).parent().children().index($(this));
            var row = $(this).parent().parent().children().index($(this).parent());

            if (col === 3) {
                var id = $('#table tbody tr:eq(' + row + ')').find("td a").eq(0).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No",
                }).then((result) => {
                    if (result.value) {
                        deleteBanner(id);
                    }
                });
            }                       

        });
    });
    
    function deleteBanner(id){
        $.ajax({
            type: "POST",
            url: "/delete_banner",
            data: {
                id: id
            },
            success: function (result) {
               location.reload();            
            },
            error: function (result) {
                Swal.fire({
                    type: 'error',
                    title: 'Banner delete failed...',
                    text: ''
                });
            }
        });
    }
</script>
</body>
</html>
