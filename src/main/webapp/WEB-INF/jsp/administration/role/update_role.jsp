<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
              <li class="breadcrumb-item active">Update Role</li>
            </ol>
          </div>
        </div>
                  
        <span id="msg"></span>
        
      </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">                                                             
                <div class="card card-secondary">                   
                    <form id="form" action="" method="post"> 
                        <div class="card-body">
                            <input type="hidden" class="form-control" id="id" name="id" value="${role.id}">
                            
                            <div class="form-group">
                                <label>ROLE NAME:</label>
                                
                                <input type="text" class="form-control" id="name" name="name" value="${role.name}" placeholder="ROLE NAME" autofocus>
                            </div>  
                            
                            <div class="form-group">
                                <label>MODULES:</label>

                                <select class="form-control select2" id="modules" name="modules[]" multiple="multiple" data-placeholder="Select a Modules" style="width: 100%;">
                                    <c:forEach items="${module}" var="lst">
                                        <option value="${lst.id}">${lst.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                                
                        <div class="card-footer">
                            <button type="submit" id="updateBtn" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () { 
        $(".administration").removeClass("treeview").addClass("treeview menu-open");
        $("#administration-menu").css('display', 'block');
        $("#role").removeClass("has-treeview").addClass("has-treeview menu-open");
        $("#role-menu").css('display', 'block');
        $("#view_role").addClass("active");
        
        $('.select2').select2();
        
        var selectModules = ${role.modules};        
        var values = new Array();       
                
        for (var i = 0; i < selectModules.length; i++) {
            values.push(selectModules[i].id);
        }
        
        $("#modules").val(values).trigger('change');
        
        $('#form').validate({
            rules: {
                name: {
                    required: true
                },
                modules: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: "../update_role",         
                    data: $('#form').serialize(),
                    success: function (result) {
                        if (result == "200") {
                            $('#msg').append('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i>Role updated Successfully</h4></div>');
                        } else {
                            $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i> Role updated Failed</h4></div>');
                        }
                    },
                    error: function (result) {
                        $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i> Role updated Failed</h4></div>');                       
                    }
                });
            }
        });       
    });
</script>
</body>
</html>
