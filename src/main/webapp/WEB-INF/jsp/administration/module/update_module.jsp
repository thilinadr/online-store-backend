<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Module</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
              <li class="breadcrumb-item active">Update Module</li>
            </ol>
          </div>
        </div>
                  
        <span id="msg"></span>
        
      </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">                                                             
                <div class="card card-secondary">                   
                    <form id="form" method="post"> 
                        <div class="card-body">
                            <input type="hidden" class="form-control" id="id" name="id" value="${module.id}">
                            
                            <div class="form-group">
                                <label>MODULE NAME:</label>
                                
                                <input type="text" class="form-control" id="name" name="name" value="${module.name}" autofocus>
                            </div>                                                      
                        </div>
                                
                        <div class="card-footer">
                            <button type="submit" id="updateBtn" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {    
        $(".administration").removeClass("treeview").addClass("treeview menu-open");
        $("#administration-menu").css('display', 'block');
        $("#module").removeClass("has-treeview").addClass("has-treeview menu-open");
        $("#module-menu").css('display', 'block');
        $("#view_module").addClass("active");
        
        $('#form').validate({
            rules: {
                name: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: "../update_module",         
                    contentType: "application/json",
                    data: JSON.stringify($('#form').serializeJSON()),
                    success: function (result) {
                        if (result == "200") {
                            $('#msg').append('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i>Module updated Successfully</h4></div>');
                        } else {
                            $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i> Module updated Failed</h4></div>');
                        }
                    },
                    error: function (result) {
                        $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">*</button><h4><i class="icon fa fa-check"></i> Module updated Failed</h4></div>');                       
                    }
                });
            }
        });       
    });
</script>
</body>
</html>
