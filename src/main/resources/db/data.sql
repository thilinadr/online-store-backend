INSERT INTO `apparel360`.`admin_module` (`id`, `name`) VALUES ('1', 'administration');
INSERT INTO `apparel360`.`admin_module` (`id`, `name`) VALUES ('2', 'category');
INSERT INTO `apparel360`.`admin_module` (`id`, `name`) VALUES ('3', 'product');

INSERT INTO `apparel360`.`admin_role` (`id`, `name`) VALUES ('1', 'SUPER_ADMIN_USER');

INSERT INTO `apparel360`.`admin_role_modules` (`admin_role_id`, `modules_id`) VALUES ('1', '1');
INSERT INTO `apparel360`.`admin_role_modules` (`admin_role_id`, `modules_id`) VALUES ('1', '2');
INSERT INTO `apparel360`.`admin_role_modules` (`admin_role_id`, `modules_id`) VALUES ('1', '3');

INSERT INTO `apparel360`.`admin_user` (`id`, `password`, `username`, `admin_role_id`) VALUES ('1', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '1');

INSERT INTO `apparel360`.`customer` (`id`, `address`, `contact`, `email`, `name`, `password`, `username`) VALUES ('1', 'Sri Lanka', '0767066485', 'hks.lakshitha@gmail.com', 'sachin', '$2a$10$y0GfoWRfKtPettFGOSvZ5udOCBpaPeZiJgu.lSe3S5bZSx1TZByxG', 'sachin');

INSERT INTO `apparel360`.`company` (`id`, `address`, `contact`, `email`, `latitude`, `longitude`, `name`) VALUES ('1', 'Apparel 360 (Pvt) Ltd, No. 33, 1st Floor,Horana Road, Eluwila,Panadura', '0112233445', 'apparel360@gmail.com', '6.709225', '79.925732', 'Apparel 360 (Pvt) Ltd');