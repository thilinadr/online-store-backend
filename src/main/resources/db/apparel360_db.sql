DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(500) NOT NULL,
    username VARCHAR(150) NOT NULL,
    password VARCHAR(15) NOT NULL,
    address VARCHAR(150) NOT NULL,
    contact VARCHAR(11) NOT NULL,
    email VARCHAR(150) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS category;

CREATE TABLE category(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(150) NOT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS sub_category;

CREATE TABLE sub_category(
    id BIGINT NOT NULL AUTO_INCREMENT,
    category_id BIGINT NOT NULL,
    name VARCHAR(150) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_category_id` FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS offer;

CREATE TABLE offer(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(150) NOT NULL,
    percentage BIGINT NOT NULL,
    availability BOOL NOT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS product_size;

CREATE TABLE product_size(
    id BIGINT NOT NULL AUTO_INCREMENT,
    product_id BIGINT NOT NULL,
    name VARCHAR(50) NOT NULL,
    qty BIGINT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_product_id` FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS product;

CREATE TABLE product(
    id BIGINT NOT NULL AUTO_INCREMENT,
    sub_category_id BIGINT NOT NULL,
    offer_id BIGINT NULL,
    name VARCHAR(150) NOT NULL,
    description VARCHAR(500) NOT NULL,
    rating VARCHAR(50),
    image VARCHAR(150) NOT NULL,
    price DECIMAL NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_sub_category_id` FOREIGN KEY (offer_id) REFERENCES sub_category (id) ON DELETE CASCADE,
    CONSTRAINT `FK_offer_id` FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS orders;

CREATE TABLE orders(
    id BIGINT NOT NULL AUTO_INCREMENT,
    customer_id BIGINT NOT NULL,
    order_date TIMESTAMP NOT NULL,
    amount DECIMAL NOT NULL,
    street_address1 VARCHAR(150),
    street_address2 VARCHAR(150),
    city VARCHAR(30),
    postal_code VARCHAR(15),
    PRIMARY KEY (id),
    CONSTRAINT `FK_customer_id` FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS order_item;

CREATE TABLE order_item(
    id BIGINT NOT NULL AUTO_INCREMENT,
    item_id BIGINT NOT NULL,
    order_id BIGINT NOT NULL,
    qty BIGINT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_item_id` FOREIGN KEY (item_id) REFERENCES product (id) ON DELETE CASCADE,
    CONSTRAINT `FK_order_id` FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS payment;

CREATE TABLE payment(
    id BIGINT NOT NULL AUTO_INCREMENT,
    orders_id BIGINT NOT NULL,
    payment_method VARCHAR(150),
    amount DECIMAL NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_orders_id` FOREIGN KEY (orders_id) REFERENCES orders (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS company;

CREATE TABLE company (
    id VARCHAR(40) NOT NULL,
    name VARCHAR(50) NOT NULL,
    address VARCHAR(200) NOT NULL,
    contact VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL,
    longitude VARCHAR(10) NOT NULL,
    latitude VARCHAR(10) NOT NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE banner (
    id VARCHAR(40) NOT NULL,
    company_id VARCHAR(50) NOT NULL,
    description VARCHAR(100) NULL,
    image VARCHAR(100) NOT NULL,
    url VARCHAR(100) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `FK_company_id` FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `admin_module` (
	`id` VARCHAR(255) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `UK_onbo5p7lmt8504n73c10j6trx` (`name`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

CREATE TABLE `admin_role` (
	`id` VARCHAR(255) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `UK_g23ueq3j5ulkmbye5gf5a0r82` (`name`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

CREATE TABLE `admin_role_modules` (
	`admin_role_id` VARCHAR(255) NOT NULL,
	`modules_id` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`admin_role_id`, `modules_id`),
	INDEX `FKbc8q3t4sthm3y79bbaap1234p` (`modules_id`),
	CONSTRAINT `FKbbbjwcfdf44fghkephtxc80rt` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_role` (`id`),
	CONSTRAINT `FKbc8q3t4sthm3y79bbaap1234p` FOREIGN KEY (`modules_id`) REFERENCES `admin_module` (`id`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

CREATE TABLE `admin_user` (
	`id` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`username` VARCHAR(255) NOT NULL,
	`admin_role_id` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `UK_lvod9bfm438ex1071ku1glb70` (`username`),
	INDEX `FKb7tsen1me6klqlyj0u1mr8qrf` (`admin_role_id`),
	CONSTRAINT `FKb7tsen1me6klqlyj0u1mr8qrf` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_role` (`id`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;
