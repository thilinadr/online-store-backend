var stompClient = null;

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        //console.log('Connected: ' + frame);
       
        stompClient.subscribe('/topic/url', function (url) {
            var urlData = JSON.parse(url.body);
                         
            window.open("http://"+urlData.url, "" , "width="+urlData.width+",height="+urlData.height+"");
        });
        
        stompClient.subscribe('/topic/fileOpen', function (url) {
            var urlData = JSON.parse(url.body);
             
            window.open("../../file?name="+urlData.url, "" , "width="+urlData.width+",height="+urlData.height+"");
        });

        stompClient.subscribe('/topic/detector', function (detector) {
            var detectorData = JSON.parse(detector.body);

            for (var i = 0; i < detectorData.length; i++) {
                var detectorModel = detectorData[i];
                $("#"+detectorModel.detector).attr("src","/resources/safemesh/dist/img/"+detectorModel.color + ".png");
            }
        });
        
        stompClient.subscribe('/topic/sensor', function (detector) {
            var detectorData = JSON.parse(detector.body);
            
            if(detectorData.length === 0){
                $(".fire-box").val("");
                $(".ims-box").val("");
                $( ".alert-box" ).removeClass( "badge-danger" );
                $( ".alert-box" ).text( "Normal" );
                $( ".ims-alert-box" ).removeClass( "badge-danger" );
                $( ".ims-alert-box" ).text( "Normal" );
                $( '#floorPlanModal' ).modal( 'hide' );
            }else{
                $(".fire-box").val("");
                $(".ims-box").val("");
                
                                               
                for (var i = 0; i < detectorData.length; i++) {
                    var detectorModel = detectorData[i];
                    
                    if(detectorModel.type == "FIRE"){
                        
                        $( ".alert-box" ).addClass( "badge-danger" );
                        $( ".alert-box" ).text( "Fire" );
                
                        var txt = $(".fire-box");
                    
                        txt.val( txt.val() +detectorModel.detector+" is firing\n");
                    }
                    
                    if(detectorModel.type == "DIRECT"){
                        
                        $( ".ims-alert-box" ).addClass( "badge-danger" );
                        $( ".ims-alert-box" ).text( "Fire" );
                        
                        var txt = $(".ims-box");
                    
                        txt.val( txt.val() +detectorModel.detector+" is firing\n");
                    }
                    
                }
            }
        });

        stompClient.subscribe('/topic/line', function (line) {
            var lineData = JSON.parse(line.body);

            for (var i = 0; i < lineData.length; i++) {
                var lineModel = lineData[i];

                $("#"+lineModel.line).css('border-left', "1px solid "+lineModel.color);
                $("#"+lineModel.line).css('border-color', lineModel.color);

            }

        });

        stompClient.subscribe('/topic/door', function (door) {
            var doorData = JSON.parse(door.body);

            for (var i = 0; i < doorData.length; i++) {
                var doorModel = doorData[i];

                if(doorModel.color === 'green'){
                    $("#"+doorModel.door).css("background-color", "#00b050");
                }else{
                    $("#"+doorModel.door).css("background-color", doorModel.color);
                }
            }

        });
        
        stompClient.subscribe('/topic/transactionLog', function (transactionLog) { 
            var transactionLogData = JSON.parse(transactionLog.body);
            
            $(".log").html("");
            
            for (var i = 0; i < transactionLogData.length; i++) {
                var transactionLogMessage = transactionLogData[i][0];
                
                $(".log").append("<br>"+transactionLogMessage);
            }
            
            var tabled = $('#transactionLogTable').DataTable();
            tabled.destroy();

            var newtable = $('#transactionLogTable').DataTable({
                "pageLength": 10,
                'ordering': false,
                "data":JSON.parse(transactionLog.body)
            });
        });
        
        stompClient.subscribe('/topic/firealarmalert', function (alarm) {
            var firealarmData = JSON.parse(alarm.body);
            
            $(".firealarm-box").val("");
            
            for (var i = 0; i < firealarmData.length; i++) {
                var firebaseQuestionModel = firealarmData[i];
                var txt = $(".firealarm-box");

                txt.val( txt.val() +firebaseQuestionModel.clientNumber+":"+firebaseQuestionModel.message+"\n");
            }                    
        });
        
        stompClient.subscribe('/topic/firealarmreply', function (reply) {
            var replyData = JSON.parse(reply.body);
                         
            $(".firealarmreply-box").val("");
          
            for (var i = 0; i < replyData.length; i++) {
                var firebaseClientResponseModel = replyData[i];
                var txt = $(".firealarmreply-box");

                txt.val( txt.val() +firebaseClientResponseModel.clientNumber+":"+firebaseClientResponseModel.message+"\n");
            }
        });
        
        stompClient.subscribe('/topic/evacuation', function (ePaperDeviceMessage) {
            var ePaperDeviceMessageData = JSON.parse(ePaperDeviceMessage.body);
                                     
            $(".evacuate-box").val("");
          
            for (var i = 0; i < ePaperDeviceMessageData.length; i++) {
                var ePaperDeviceMessageModel = ePaperDeviceMessageData[i];
                var txt = $(".evacuate-box");

                txt.val( txt.val() +ePaperDeviceMessageModel.topicName+":"+ePaperDeviceMessageModel.message+"\n");
            }
        });
        
        stompClient.subscribe('/topic/floorplan', function (floorPlan) {
            var floorPlanData = JSON.parse(floorPlan.body);
                         
            $('#floorPlanModal').modal('show');
            
            $(".floorplanbody").html('');
            
            $(".floorplanbody").append('<img id="plan" src="">');
            
            $("#plan").attr("src","/uploads/"+floorPlanData.imageName);
            
            $.ajax({
                type: "GET",
                url: "/search_plan_element/"+floorPlanData.floorPlanId,
                success: function (result) {

                    $.each(result, function (i, o) {
                        var type = o.type;

                        if(type === "SENSOR"){
                            var element = $('<img src="/resources/safemesh/dist/img/green.png" id="'+o.name+'" data-type="SENSOR" data-id="'+o.planElementId+'" data-name="'+o.name+'" class="draggable" data-toggle="tooltip" title="'+o.name+'" style="position:absolute;display: inline-block;"></>');
                            element.css({left: parseInt(o.xCoordinate - 186), top: parseInt(o.yCoordinate - 5)});
                            $(".floorplanbody").append(element);
                        }

                        if(type === "door"){
                            var element = $('<div id="'+o.name+'" data-type="door" data-id="'+o.planElementId+'" data-name="'+o.name+'" class="door draggable" data-toggle="tooltip" title="'+o.name+'" style="position:absolute;display: inline-block;background-color: #00b050;height:26px;width: 25px;"></div>');
                            /*var element = $('<div id="'+o.name+'" data-type="door" data-id="'+o.planElementId+'" data-name="'+o.name+'" class="draggable" data-toggle="tooltip" title="'+o.name+'" style="position:absolute;display: inline-block;"><img src="../../resources/img/green.png"/></div>');*/
                            element.css({left: parseInt(o.xCoordinate - 186), top: parseInt(o.yCoordinate - 4)});
                            $(".floorplanbody").append(element);    
                        }

                        if(type === "path"){
                            var element = $('<div id="'+o.name+'" data-type="escape" data-id="'+o.planElementId+'" data-name="'+o.name+'" class="box draggable" data-toggle="tooltip" title="'+o.name+'" style="border-left: 1px solid green;height: 28px;position: absolute;display: inline-block;margin-left: 12px;top: '+parseInt(o.yCoordinate - 5)+'px;left: '+parseInt(o.xCoordinate - 187)+'px;transform: rotate('+o.degrees+'deg);height: '+o.height+'px;"></div>');
                            //element.css({left: parseInt(o.xCoordinate), top: parseInt(o.yCoordinate)});
                            $(".floorplanbody").append(element);
                        }
                    });
                },
                error: function (result) {
                }
            });
        });

    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

$(document).ready(function() {
    connect();
});

