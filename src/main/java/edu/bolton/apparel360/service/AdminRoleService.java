/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.AdminRoleDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface AdminRoleService {
    public boolean save(AdminRoleDto adminRoleDto);
    public List<AdminRoleDto> readAll();
    public AdminRoleDto findById(String id);
    public boolean update(AdminRoleDto adminRoleDto);
    public void deleteById(String id);
}
