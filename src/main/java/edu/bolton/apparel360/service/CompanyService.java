/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.CompanyDto;

/**
 *
 * @author Sachin
 */
public interface CompanyService {
    public CompanyDto findById(String id);
}
