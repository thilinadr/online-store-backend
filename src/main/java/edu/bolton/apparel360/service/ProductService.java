/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.ProductDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface ProductService {
    public boolean save(ProductDto productDto);
    public List<ProductDto> readAll();
    public List<ProductDto> findBySubCategoryId(String subCategoryId);
    public ProductDto findById(String id);
    public boolean update(ProductDto productDto);
    public void deleteById(String id);
    public long count();
}
