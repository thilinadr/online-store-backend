/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.AdminUserDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface AdminUserService {
    public boolean save(AdminUserDto adminUserDto);
    public AdminUserDto findByUsernameAndPassword(String username,String password);
    public List<AdminUserDto> readAll();
    public AdminUserDto findById(String id);
    public boolean update(AdminUserDto adminUserDto);
    public void deleteById(String id);
}
