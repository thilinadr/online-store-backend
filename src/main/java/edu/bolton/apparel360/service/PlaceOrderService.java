package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.OrderDto;
import edu.bolton.apparel360.dto.OrdersDto;
import java.util.List;

/**
 * @author thisu96
 * @Date 20/07/2020
 * @Time 18:33
 */
public interface PlaceOrderService {
    public boolean placeOrder(OrdersDto dto);
    public List<OrderDto> readOrdersByCustomerId(long customerId);
    public long count();
    public String sales();
}
