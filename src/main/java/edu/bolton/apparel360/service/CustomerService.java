package edu.bolton.apparel360.service;


import edu.bolton.apparel360.dto.CustomerDto;

import java.util.List;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:00
 */
public interface CustomerService {
    public CustomerDto save(CustomerDto customer);
    public List<CustomerDto> readAll();
    public CustomerDto findById(Long id);
    public Long update(Long id,CustomerDto customer);
    public Long deleteById(Long id);
    public long count();
}
