/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.AdminModuleDto;
import edu.bolton.apparel360.dto.AdminRoleDto;
import edu.bolton.apparel360.dto.AdminUserDto;
import edu.bolton.apparel360.model.AdminModule;
import edu.bolton.apparel360.model.AdminRole;
import edu.bolton.apparel360.model.AdminUser;
import edu.bolton.apparel360.repository.AdminUserRepository;
import edu.bolton.apparel360.service.AdminUserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class AdminUserServiceImpl implements AdminUserService{
    @Autowired
    private AdminUserRepository adminUserRepository;
    
    @Override
    public boolean save(AdminUserDto adminUserDto) {
        adminUserDto.setId(UUID.randomUUID().toString());
        
        AdminUser adminUser = adminUserRepository.save(getAdminUser(adminUserDto));
        
        if(adminUser == null){
            return false;
        }
        
        return true;
    }
    
    @Override
    public AdminUserDto findByUsernameAndPassword(String username, String password) {
        AdminUser adminUser = adminUserRepository.findByUsernameAndPassword(username, password);
        
        if(adminUser != null){
            return getAdminUserDto(adminUser);
        }
        
        return null;
    }
    
    public AdminUser getAdminUser(AdminUserDto adminUserDto){
        AdminUser adminUser = new AdminUser();
        
        adminUser.setId(adminUserDto.getId());
        adminUser.setUsername(adminUserDto.getUsername());
        adminUser.setPassword(adminUserDto.getPassword());
        adminUser.setAdminRole(getAdminRole(adminUserDto.getAdminRole()));
        
        return adminUser;
    }
    
    public AdminUserDto getAdminUserDto(AdminUser adminUser){
        AdminUserDto adminUserDto = new AdminUserDto();
        
        adminUserDto.setId(adminUser.getId());
        adminUserDto.setUsername(adminUser.getUsername());
        adminUserDto.setPassword(adminUser.getPassword());
        adminUserDto.setAdminRole(getAdminRoleDto(adminUser.getAdminRole()));
        
        return adminUserDto;
    }
    
    public AdminRole getAdminRole(AdminRoleDto adminRoleDto){
        AdminRole adminRole = new AdminRole();
        
        adminRole.setId(adminRoleDto.getId());
        adminRole.setName(adminRoleDto.getName());
        
        Set<AdminModule> adminModules = new HashSet<>();
        
        Set<AdminModuleDto> adminModuleDtos = adminRoleDto.getModules();
        
        for (AdminModuleDto adminModuleDto : adminModuleDtos) {
            adminModules.add(getAdminModule(adminModuleDto));
        }
        
        adminRole.setModules(adminModules);
        
        return adminRole;        
    }
    
    public AdminRoleDto getAdminRoleDto(AdminRole adminRole){
        AdminRoleDto adminRoleDto = new AdminRoleDto();
        
        adminRoleDto.setId(adminRole.getId());
        adminRoleDto.setName(adminRole.getName());
        
        Set<AdminModuleDto> adminModuleDtos = new HashSet<>();
        
        Set<AdminModule> adminModules = adminRole.getModules();
        
        for (AdminModule adminModule : adminModules) {
            adminModuleDtos.add(getAdminModuleDto(adminModule));
        }
        
        adminRoleDto.setModules(adminModuleDtos);
        
        return adminRoleDto;
    }
    
    public AdminModule getAdminModule(AdminModuleDto adminModuleDto){
        AdminModule adminModule = new AdminModule();
        
        adminModule.setId(adminModuleDto.getId());
        adminModule.setName(adminModuleDto.getName());
        
        return adminModule;
    }
    
    public AdminModuleDto getAdminModuleDto(AdminModule adminModule){
        AdminModuleDto adminModuleDto = new AdminModuleDto();
        
        adminModuleDto.setId(adminModule.getId());
        adminModuleDto.setName(adminModule.getName());
       
        return adminModuleDto;
    }

    @Override
    public List<AdminUserDto> readAll() {
        List<AdminUserDto> adminUserDtoList = new ArrayList<>();
        
        Iterable<AdminUser> adminUsers = adminUserRepository.findAll();
        
        for (AdminUser adminUser : adminUsers) {
            adminUserDtoList.add(getAdminUserDto(adminUser));
        }
        
        return adminUserDtoList;
    }

    @Override
    public AdminUserDto findById(String id) {
        return getAdminUserDto(adminUserRepository.findById(id).get());
    }

    @Override
    public boolean update(AdminUserDto adminUserDto) {
        AdminUser adminUser = adminUserRepository.save(getAdminUser(adminUserDto));
        
        if(adminUser == null){
            return false;
        }
        
        return true;
    }

    @Override
    public void deleteById(String id) {
        adminUserRepository.deleteById(id);
    }
}
