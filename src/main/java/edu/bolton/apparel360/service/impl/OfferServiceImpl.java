/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.OfferDto;
import edu.bolton.apparel360.dto.ProductDto;
import edu.bolton.apparel360.dto.ProductImageDto;
import edu.bolton.apparel360.dto.ProductSizeDto;
import edu.bolton.apparel360.dto.SubCategoryDto;
import edu.bolton.apparel360.model.Offer;
import edu.bolton.apparel360.model.Product;
import edu.bolton.apparel360.model.ProductImage;
import edu.bolton.apparel360.model.ProductSize;
import edu.bolton.apparel360.model.SubCategory;
import edu.bolton.apparel360.repository.OfferRepository;
import edu.bolton.apparel360.service.OfferService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class OfferServiceImpl implements OfferService{
    @Autowired
    private OfferRepository offerRepository;
    
    @Override
    public OfferDto findByProductId(String productId) {
        Offer offer = offerRepository.findByProductId(productId);
        if(offer != null){
            return getOfferDto(offerRepository.findByProductId(productId));
        }
        
        return null;
    }
    
    public OfferDto getOfferDto(Offer offer) {
        OfferDto offerDto = new OfferDto();
        
        offerDto.setId(offer.getId());
        offerDto.setName(offer.getName());
        offerDto.setPercentage(offer.getPercentage());
        offerDto.setAvailability(offer.isAvailability());
               
        return offerDto;
    }
    
    public OfferDto getOfferDtoFull(Offer offer) {
        OfferDto offerDto = new OfferDto();
        
        offerDto.setId(offer.getId());
        offerDto.setName(offer.getName());
        offerDto.setPercentage(offer.getPercentage());
        offerDto.setAvailability(offer.isAvailability());
        
        if(offer.getProduct() != null){
            offerDto.setProductDto(getProductDto(offer.getProduct()));
        }

        return offerDto;
    }
    
    public ProductDto getProductDto(Product product){
        ProductDto productDto = new ProductDto();
        
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setPrice(product.getPrice());
                
        SubCategoryDto subCategoryDto = getSubCategoryDto(product.getSubCategory());
        
        productDto.setSubCategory(subCategoryDto);
        
        Set<ProductImageDto> productImagesDto = new HashSet<>();
        
        Set<ProductImage> productImages = product.getImages();
        
        for (ProductImage productImage : productImages) {
            ProductImageDto productImageDto = getProductImageDto(productImage);
                        
            productImagesDto.add(productImageDto);
        }
                                        
        productDto.setImages(productImagesDto);
        
        Set<ProductSizeDto> productSizesDto = new HashSet<>();
        
        Set<ProductSize> productSizes = product.getProductsizes();
        
        for (ProductSize productSize : productSizes) {
            ProductSizeDto productSizeDto = getProductSizeDto(productSize);
            
            productSizesDto.add(productSizeDto);
        }
        
        productDto.setProductsizes(productSizesDto);
        
        Offer offer = product.getOffer();
        
        if(offer != null){
            OfferDto offerDto = getOfferDto(offer);
            
            productDto.setOffer(offerDto);
        }
        
        return productDto;
    }
    
    public SubCategoryDto getSubCategoryDto(SubCategory subCategory) {
        SubCategoryDto subCategoryDto = new SubCategoryDto();

        subCategoryDto.setId(subCategory.getId());
        subCategoryDto.setName(subCategory.getName());

        return subCategoryDto;
    }
    
    public ProductImageDto getProductImageDto(ProductImage productImage) {
        ProductImageDto productImageDto = new ProductImageDto();
        
        productImageDto.setId(productImage.getId());
        productImageDto.setName(productImage.getName());
        
        return productImageDto;
    }
    
    public ProductSizeDto getProductSizeDto(ProductSize productSize) {
        ProductSizeDto productSizeDto = new ProductSizeDto();

        productSizeDto.setId(productSize.getId());
        productSizeDto.setName(productSize.getName());
        productSizeDto.setQty(productSize.getQty());

        return productSizeDto;
    }

    @Override
    public List<OfferDto> readAll() {
        List<OfferDto> offerDtoList = new ArrayList<>();
        Iterable<Offer> offers = offerRepository.findAll();
        
        for (Offer offer : offers) {
            offerDtoList.add(getOfferDtoFull(offer));
        }
        
        return offerDtoList;
    }
       
}
