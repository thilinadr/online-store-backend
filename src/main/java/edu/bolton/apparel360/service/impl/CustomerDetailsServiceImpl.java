/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.CustomerDto;
import edu.bolton.apparel360.model.Customer;
import edu.bolton.apparel360.repository.CustomerRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class CustomerDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private CustomerRepository userRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = userRepository.findByUsername(username);
        
        if (customer == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        
        return new org.springframework.security.core.userdetails.User(customer.getUsername(), customer.getPassword(),
                new ArrayList<>());
    }

    public CustomerDto findCustomer(String userName) throws UsernameNotFoundException {
        Customer c = userRepository.findByUsername(userName);

        if (c == null) {
            throw new UsernameNotFoundException("User not found with username: " + userName);
        }

        CustomerDto dto = new CustomerDto();
        dto.setId(c.getId());
        dto.setName(c.getName());
        dto.setUsername(c.getUsername());
        dto.setAddress(c.getAddress());
        dto.setContact(c.getContact());
        dto.setEmail(c.getEmail());
        return dto;
    }
    
}
