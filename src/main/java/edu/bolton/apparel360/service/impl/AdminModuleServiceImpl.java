/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.AdminModuleDto;
import edu.bolton.apparel360.model.AdminModule;
import edu.bolton.apparel360.repository.AdminModuleRepository;
import edu.bolton.apparel360.service.AdminModuleService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class AdminModuleServiceImpl implements AdminModuleService{
    @Autowired
    private AdminModuleRepository adminModuleRepository;
    
    @Override
    public boolean save(AdminModuleDto adminModuleDto){
        adminModuleDto.setId(UUID.randomUUID().toString());
        
        AdminModule adminModule = adminModuleRepository.save(getAdminModule(adminModuleDto));
        
        if(adminModule == null){
            return false;
        }
        
        return true;
    }
        
    @Override
    public List<AdminModuleDto> readAll() {
        List<AdminModuleDto> adminModuleDtoList = new ArrayList<>();
        
        Iterable<AdminModule> adminModules = adminModuleRepository.findAll();
        
        for (AdminModule adminModule : adminModules) {
            adminModuleDtoList.add(getAdminModuleDto(adminModule));
        }
        
        return adminModuleDtoList;
    }
        
    @Override
    public AdminModuleDto findById(String id) {
        return getAdminModuleDto(adminModuleRepository.findById(id).get());
    }

    @Override
    public boolean update(AdminModuleDto adminModuleDto) {
        AdminModule adminModule = adminModuleRepository.save(getAdminModule(adminModuleDto));
        
        if(adminModule == null){
            return false;
        }
        
        return true;
    }

    @Override
    public void deleteById(String id) {
        adminModuleRepository.deleteById(id);
    }
    
    public AdminModule getAdminModule(AdminModuleDto adminModuleDto){
        AdminModule adminModule = new AdminModule();
        
        adminModule.setId(adminModuleDto.getId());
        adminModule.setName(adminModuleDto.getName());
        
        return adminModule;
    }
    
    public AdminModuleDto getAdminModuleDto(AdminModule adminModule){
        AdminModuleDto adminModuleDto = new AdminModuleDto();
        
        adminModuleDto.setId(adminModule.getId());
        adminModuleDto.setName(adminModule.getName());
       
        return adminModuleDto;
    }
}
