/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.BannerDto;
import edu.bolton.apparel360.dto.CompanyDto;
import edu.bolton.apparel360.dto.SubCategoryDto;
import edu.bolton.apparel360.model.Banner;
import edu.bolton.apparel360.model.Company;
import edu.bolton.apparel360.repository.CompanyRepository;
import edu.bolton.apparel360.service.CompanyService;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class CompanyServiceImpl implements CompanyService{
    @Autowired
    private CompanyRepository companyRepository;
    
    @Override
    public CompanyDto findById(String id) {
        return getCompanyDto(companyRepository.findById(id).get());
    }
    
    public CompanyDto getCompanyDto(Company company){
        CompanyDto companyDto = new CompanyDto();
        
        companyDto.setId(company.getId());
        companyDto.setName(company.getName());
        companyDto.setAddress(company.getAddress());
        companyDto.setContact(company.getContact());
        companyDto.setEmail(company.getEmail());
        companyDto.setLongitude(company.getLongitude());
        companyDto.setLatitude(company.getLatitude());
        
        Set<BannerDto> bannerDtos = new HashSet<>();
        
        for (Banner banner : company.getBanners()) {
            bannerDtos.add(getBannerDto(banner));
        }
        
        companyDto.setBanners(bannerDtos);
        
        return companyDto;
    }
    
    public BannerDto getBannerDto(Banner banner){
        BannerDto bannerDto = new BannerDto();
        
        bannerDto.setId(banner.getId());
        bannerDto.setImage(banner.getImage());
        bannerDto.setDescription(banner.getDescription());
        bannerDto.setUrl(banner.getUrl());
        
        return bannerDto;
    }
}
