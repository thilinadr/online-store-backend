package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.exception.DuplicateException;
import edu.bolton.apparel360.response.Messages;
import edu.bolton.apparel360.dto.CustomerDto;
import edu.bolton.apparel360.exception.DataNotFoundException;
import edu.bolton.apparel360.exception.TransactionConflictException;
import edu.bolton.apparel360.model.Customer;
import edu.bolton.apparel360.repository.CustomerRepository;
import edu.bolton.apparel360.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:03
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    private Logger logger = LogManager.getLogger(CustomerServiceImpl.class);

    @Override
    public CustomerDto save(CustomerDto customer) {
        if (customer == null) {
            throw new DataNotFoundException("Customer Details Are Not Found..");
        }
        if (customerRepository.findByUsername(customer.getUsername()) != null) {
            throw new DuplicateException("This Username Already in Database");
        }
        Customer cust = new Customer();
        cust.setName(customer.getName());
        cust.setUsername(customer.getUsername());
        cust.setAddress(customer.getAddress());
        cust.setContact(customer.getContact());
        cust.setEmail(customer.getEmail());
        cust.setPassword(bcryptEncoder.encode(customer.getPassword()));
        logger.info("add new Customer , customer:{}", customer);
        persistEntity(cust);
        return getCustomerDto(cust);
    }

    @Override
    public List<CustomerDto> readAll() {
        Iterable<Customer> all = customerRepository.findAll();
        List<CustomerDto> targetCustomerList = new ArrayList<>();
        for (Customer c : all) {
            CustomerDto dto = new CustomerDto();
            dto.setName(c.getName());
            dto.setUsername(c.getUsername());
            dto.setAddress(c.getAddress());
            dto.setContact(c.getContact());
            dto.setEmail(c.getEmail());
            dto.setPassword(c.getPassword());
            targetCustomerList.add(dto);
        }
        logger.info("get Customer Detils , targetCustomerList:{}", targetCustomerList);
        return targetCustomerList;
    }

    @Override
    public CustomerDto findById(Long id) {
        if (id == null) {
            throw new DataNotFoundException("Customer Id Is Required...");
        }
        Customer c = customerRepository.findById(id).get();
        CustomerDto dto = new CustomerDto();
        dto.setName(c.getName());
        dto.setUsername(c.getUsername());
        dto.setAddress(c.getAddress());
        dto.setContact(c.getContact());
        dto.setEmail(c.getEmail());
        logger.info("get Customer Detils ,id:{}, dto:{}",id, dto);
        dto.setPassword(c.getPassword());
        return dto;
    }

    @Override
    public Long update(Long id,CustomerDto customer) {
        if (id == null) {
            throw new DataNotFoundException("Customer Id Is Required...");
        }
        Customer c = customerRepository.findById(id).get();
        c.setName(customer.getName());
        c.setUsername(customer.getUsername());
        c.setAddress(customer.getAddress());
        c.setContact(customer.getContact());
        c.setEmail(customer.getEmail());
        logger.info("Update Customer Detils ,id:{}, dto:{}",id, c);
        return persistEntity(c).getId();
    }

    @Override
    public Long deleteById(Long id) {
        if (id == null) {
            throw new DataNotFoundException("Customer Id Is Required...");
        }
        Customer customer = customerRepository.findById(id).get();
        
        if (customer == null) { 
            throw new DataNotFoundException("Request Customer Not Found...");
        }
        
        customerRepository.deleteById(id);
        
        return id;
    }

    private CustomerDto getCustomerDto(Customer customer) {
        CustomerDto dto = new CustomerDto();
        dto.setName(customer.getName());
        dto.setUsername(customer.getUsername());
        dto.setAddress(customer.getAddress());
        dto.setContact(customer.getContact());
        dto.setEmail(customer.getEmail());
        dto.setPassword(customer.getPassword());
        return dto;
    }

    private Customer persistEntity(Customer customer) {
        try {
            return customerRepository.save(customer);
        } catch (ObjectOptimisticLockingFailureException e) {
            throw new TransactionConflictException(Messages.CONCURRENT_ISSUE);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    @Override
    public long count() {
        return customerRepository.count();
    }
}
