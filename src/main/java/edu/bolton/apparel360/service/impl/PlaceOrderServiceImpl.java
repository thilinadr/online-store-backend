package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.CustomerDto;
import edu.bolton.apparel360.dto.OrderDto;
import edu.bolton.apparel360.dto.OrderItemDto;
import edu.bolton.apparel360.dto.OrdersDto;
import edu.bolton.apparel360.model.*;
import edu.bolton.apparel360.repository.*;
import edu.bolton.apparel360.service.PlaceOrderService;
import edu.bolton.apparel360.util.Utility;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;

/**
 * @author thisu96
 * @Date 20/07/2020
 * @Time 18:32
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlaceOrderServiceImpl implements PlaceOrderService {
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository itemRepository;
    @Autowired
    private ProductSizeRepository productSizeRepository;
    @Autowired
    private OrderItemRepository orderDetailsRepository;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean placeOrder(OrdersDto dto) {
        Customer customer = customerRepository.findById(dto.getCustomer().getId()).get();
        System.out.println("service run");
        System.out.println(dto.getCustomer().getUsername());
        System.out.println(customer);
        Orders orders = new Orders(new Date(),
                dto.getStreetAddress1(),
                customer);
        System.out.println("service"+orders);
        Orders od = ordersRepository.save(orders);

        List<OrderItemDto> orderDetailsDTOList = dto.getOrderDetails();

        for (OrderItemDto orderItemDto : orderDetailsDTOList){
            Product product = itemRepository.findById(orderItemDto.getProduct().getId()).get();
            //product.set(ordersDetails.getItem().getQtyOnHand()-ordersDetails.getOrder_qty());
            //Product p = itemRepository.save(product);
            
            /*int orderItemQty = (int) orderItemDto.getQty();
            
            ProductSize productSize = productSizeRepository.findByName(orderItemDto.getSize());
            
            int currentQty = productSize.getQty() - orderItemQty;
            
            productSize.setQty(currentQty);
            
            productSizeRepository.save(productSize);
            */
            
            OrderItem ordersDetails = new OrderItem();
            ordersDetails.setQty(orderItemDto.getQty());
            ordersDetails.setProduct(product);
            ordersDetails.setOrders(orders);

            OrderDetail_PK orderDetail_pk = new OrderDetail_PK(product.getId(),orders.getOrder_id());
            ordersDetails.setOrderDetail_pk(orderDetail_pk);
            System.out.println("order details pk"+ orderDetail_pk);
            orderDetailsRepository.save(ordersDetails);
        }

        Payment payment = new Payment();
        payment.setOrders(od);
        payment.setPayment_method(dto.getPayment().getPaymentMethod());
        payment.setAmount(dto.getPayment().getAmount());
        paymentRepository.save(payment);

        try {
                sendMailToCustomer(orders,payment,(dto.getOrderDetails()).get(0));
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        return true;
    }

    private void sendMailToCustomer(Orders orders, Payment payment,OrderItemDto item) throws MessagingException {
        Product product = itemRepository.findById(item.getProduct().getId()).get();
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
        mimeMessageHelper.setFrom("apperal360@gmail.com");
        mimeMessageHelper.setTo(orders.getCustomer().getEmail());
        mimeMessageHelper.setSubject(orders.getCustomer().getName()+"'S" +" "+ "Order Details");
        mimeMessageHelper.setText("Item Name : "+product.getName()+"\n" + "Order Date : "+orders.getOrderDate() +"\n"+ "Order Qty : "+item.getQty() +"\n"+ "Total Ammount : " +payment.getAmount()+"\n");

        javaMailSender.send(mimeMessage);
    }

    @Override
    public List<OrderDto> readOrdersByCustomerId(long customerId) {
        List<OrderDto> ordersDtos = new ArrayList<>();
               
        List<Orders> orders = ordersRepository.findByCustomerId(customerId);
        
        for (Orders order : orders) {
            OrderDto ordersDto = new OrderDto();
            
            ordersDto.setId(order.getOrder_id());
            ordersDto.setOrderDate(order.getOrderDate());
            ordersDto.setStreetAddress1(order.getStreetAddress1());
            
            Customer customer = customerRepository.findById(customerId).get();
            
            ordersDto.setCustomer(customer);
                                    
            List<OrderItem> orderItems = orderDetailsRepository.findByOrdersOrderId(order.getOrder_id());
            
            ordersDto.setOrderDetails(orderItems);
            
            Payment payment = paymentRepository.findByOrderId(order.getOrder_id());
            
            ordersDto.setPayment(payment);
            
            ordersDtos.add(ordersDto);
        }
        
        return ordersDtos;
    }

    @Override
    public long count() {
        return ordersRepository.count();
    }

    @Override
    public String sales() {
        List<Orders> ordersList = ordersRepository.findAll();
        
        double sales = 0;
        
        for (Orders orders : ordersList) {
            Payment payment = paymentRepository.findByOrderId(orders.getOrder_id());
            
            sales+=payment.getAmount();
        }
        
        return Utility.convertDouble(sales);
    }
}
