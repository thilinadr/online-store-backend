/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.CategoryDto;
import edu.bolton.apparel360.dto.SubCategoryDto;
import edu.bolton.apparel360.model.Category;
import edu.bolton.apparel360.model.SubCategory;
import edu.bolton.apparel360.repository.CategoryRepository;
import edu.bolton.apparel360.repository.SubCategoryRepository;
import edu.bolton.apparel360.service.CategoryService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Override
    public boolean save(CategoryDto categoryDto) {
        categoryDto.setId(UUID.randomUUID().toString());

        Category category = categoryRepository.save(getCategory(categoryDto));

        if (category == null) {
            return false;
        }

        return true;
    }
    
    @Override
    public List<CategoryDto> readAll() {
        List<CategoryDto> categoryDtoList = new ArrayList<>();

        Iterable<Category> categories = categoryRepository.findAll();

        for (Category category : categories) {
            categoryDtoList.add(getCategoryDto(category));
        }
        
        return categoryDtoList;
    }

    @Override
    public CategoryDto findById(String id) {
        return getCategoryDto(categoryRepository.findById(id).get());
    }

    @Override
    public boolean update(CategoryDto categoryDto) {
        Category categoryPrevious = categoryRepository.findById(categoryDto.getId()).get();
        
        Set<SubCategory> subCategories = categoryPrevious.getSubCategories();
        
        for (SubCategory subCategory : subCategories) {
            subCategoryRepository.delete(subCategory);
        }
        
        Category category = categoryRepository.save(getCategory(categoryDto));

        if (category == null) {
            return false;
        }

        return true;
    }

    @Override
    public void deleteById(String id) {
        subCategoryRepository.deleteById(id);
    }

    public Category getCategory(CategoryDto categoryDto) {
        Category category = new Category();

        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());

        Set<SubCategory> subCategories = new HashSet<>();

        Set<SubCategoryDto> subCategorieDtos = categoryDto.getSubCategories();

        if (subCategorieDtos != null) {
            for (SubCategoryDto subCategorieDto : subCategorieDtos) {
                subCategories.add(getSubCategory(subCategorieDto));
            }

            category.setSubCategories(subCategories);
        }

        return category;
    }

    public CategoryDto getCategoryDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());

        Set<SubCategoryDto> subCategoryDtos = new HashSet<>();

        Set<SubCategory> subCategories = category.getSubCategories();

        for (SubCategory subCategory : subCategories) {
            subCategoryDtos.add(getSubCategoryDto(subCategory));
        }

        categoryDto.setSubCategories(subCategoryDtos);

        return categoryDto;
    }

    public SubCategory getSubCategory(SubCategoryDto subCategoryDto) {
        SubCategory subCategory = new SubCategory();

        subCategory.setId(subCategoryDto.getId());
        subCategory.setName(subCategoryDto.getName());

        return subCategory;
    }
    
    public SubCategoryDto getSubCategoryDto(SubCategory subCategory) {
        SubCategoryDto subCategoryDto = new SubCategoryDto();

        subCategoryDto.setId(subCategory.getId());
        subCategoryDto.setName(subCategory.getName());

        return subCategoryDto;
    }
}
