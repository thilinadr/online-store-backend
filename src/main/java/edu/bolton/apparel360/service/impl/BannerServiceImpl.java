/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.BannerDto;
import edu.bolton.apparel360.model.Banner;
import edu.bolton.apparel360.model.Company;
import edu.bolton.apparel360.repository.BannerRepository;
import edu.bolton.apparel360.repository.CompanyRepository;
import edu.bolton.apparel360.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class BannerServiceImpl implements BannerService{
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private BannerRepository bannerRepository;
    
    @Override
    public boolean save(BannerDto bannerDto) {
        Company company = companyRepository.findById("1").get();
        
        Banner banner = new Banner();
        
        banner.setId(bannerDto.getId());
        banner.setCompany(company);
        banner.setImage(bannerDto.getImage());
        banner.setDescription(bannerDto.getDescription());
        banner.setUrl(bannerDto.getUrl());
        
        Banner bannerObj = bannerRepository.save(banner);
        
        if (bannerObj == null) {
            return false;
        }

        return true;
    }

    @Override
    public void deleteById(String id) {
        bannerRepository.deleteById(id);
    }
    
}
