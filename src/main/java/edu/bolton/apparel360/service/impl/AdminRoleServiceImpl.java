/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.AdminModuleDto;
import edu.bolton.apparel360.dto.AdminRoleDto;
import edu.bolton.apparel360.model.AdminModule;
import edu.bolton.apparel360.model.AdminRole;
import edu.bolton.apparel360.repository.AdminRoleRepository;
import edu.bolton.apparel360.service.AdminRoleService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class AdminRoleServiceImpl implements AdminRoleService{
    @Autowired
    private AdminRoleRepository adminRoleRepository;
    
    @Override
    public boolean save(AdminRoleDto adminRoleDto) {
        adminRoleDto.setId(UUID.randomUUID().toString());
                        
        AdminRole adminRole = adminRoleRepository.save(getAdminRole(adminRoleDto));
        
        if(adminRole == null){
            return false;
        }
        
        return true;
    }
                
    @Override
    public List<AdminRoleDto> readAll() {
        List<AdminRoleDto> adminRoleDtoList = new ArrayList<>();
        
        Iterable<AdminRole> adminRoles = adminRoleRepository.findAll();
        
        for (AdminRole adminRole : adminRoles) {
            adminRoleDtoList.add(getAdminRoleDto(adminRole));
        }
        
        return adminRoleDtoList;
    }

    @Override
    public AdminRoleDto findById(String id) {
        return getAdminRoleDto(adminRoleRepository.findById(id).get());
    }

    @Override
    public boolean update(AdminRoleDto adminRoleDto) {
        AdminRole adminRole = adminRoleRepository.save(getAdminRole(adminRoleDto));
        
        if(adminRole == null){
            return false;
        }
        
        return true;
    }

    @Override
    public void deleteById(String id) {
        adminRoleRepository.deleteById(id);
    }
    
    public AdminRole getAdminRole(AdminRoleDto adminRoleDto){
        AdminRole adminRole = new AdminRole();
        
        adminRole.setId(adminRoleDto.getId());
        adminRole.setName(adminRoleDto.getName());
        
        Set<AdminModule> adminModules = new HashSet<>();
        
        Set<AdminModuleDto> adminModuleDtos = adminRoleDto.getModules();
        
        for (AdminModuleDto adminModuleDto : adminModuleDtos) {
            adminModules.add(getAdminModule(adminModuleDto));
        }
        
        adminRole.setModules(adminModules);
        
        return adminRole;        
    }
    
    public AdminRoleDto getAdminRoleDto(AdminRole adminRole){
        AdminRoleDto adminRoleDto = new AdminRoleDto();
        
        adminRoleDto.setId(adminRole.getId());
        adminRoleDto.setName(adminRole.getName());
        
        Set<AdminModuleDto> adminModuleDtos = new HashSet<>();
        
        Set<AdminModule> adminModules = adminRole.getModules();
        
        for (AdminModule adminModule : adminModules) {
            adminModuleDtos.add(getAdminModuleDto(adminModule));
        }
        
        adminRoleDto.setModules(adminModuleDtos);
        
        return adminRoleDto;
    }
    
    public AdminModule getAdminModule(AdminModuleDto adminModuleDto){
        AdminModule adminModule = new AdminModule();
        
        adminModule.setId(adminModuleDto.getId());
        adminModule.setName(adminModuleDto.getName());
        
        return adminModule;
    }
    
    public AdminModuleDto getAdminModuleDto(AdminModule adminModule){
        AdminModuleDto adminModuleDto = new AdminModuleDto();
        
        adminModuleDto.setId(adminModule.getId());
        adminModuleDto.setName(adminModule.getName());
       
        return adminModuleDto;
    }
}
