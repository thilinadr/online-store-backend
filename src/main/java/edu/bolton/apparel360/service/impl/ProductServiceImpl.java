/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service.impl;

import edu.bolton.apparel360.dto.OfferDto;
import edu.bolton.apparel360.dto.ProductDto;
import edu.bolton.apparel360.dto.ProductImageDto;
import edu.bolton.apparel360.dto.ProductSizeDto;
import edu.bolton.apparel360.dto.SubCategoryDto;
import edu.bolton.apparel360.model.Category;
import edu.bolton.apparel360.model.Offer;
import edu.bolton.apparel360.model.Product;
import edu.bolton.apparel360.model.ProductImage;
import edu.bolton.apparel360.model.ProductSize;
import edu.bolton.apparel360.model.SubCategory;
import edu.bolton.apparel360.repository.CategoryRepository;
import edu.bolton.apparel360.repository.OfferRepository;
import edu.bolton.apparel360.repository.ProductImageRepository;
import edu.bolton.apparel360.repository.ProductRepository;
import edu.bolton.apparel360.repository.ProductSizeRepository;
import edu.bolton.apparel360.repository.SubCategoryRepository;
import edu.bolton.apparel360.service.ProductService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SubCategoryRepository subCategoryRepository;
    @Autowired
    private ProductImageRepository productImageRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductSizeRepository productSizeRepository;
    @Autowired
    private OfferRepository offerRepository;

    @Override
    public boolean save(ProductDto productDto) {
        productDto.setId(UUID.randomUUID().toString());

        Product product = new Product();

        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setPrice(productDto.getPrice());

        SubCategory subCategory = subCategoryRepository.findById(productDto.getSubCategoryId()).get();

        product.setSubCategory(subCategory);

        Product productO = productRepository.save(product);

        if (productO == null) {
            return false;
        }

        Set<ProductImageDto> productImagesDto = productDto.getImages();

        for (ProductImageDto productImageDto : productImagesDto) {
            ProductImage productImage = getProductImage(productImageDto);
            productImage.setProduct(productO);

            productImageRepository.save(productImage);
        }

        Set<ProductSizeDto> productSizesDto = productDto.getProductsizes();

        for (ProductSizeDto productSizeDto : productSizesDto) {
            ProductSize productSize = getProductSize(productSizeDto);
            productSize.setProduct(productO);

            productSizeRepository.save(productSize);
        }

        return true;
    }

    public SubCategory getSubCategory(SubCategoryDto subCategoryDto) {
        SubCategory subCategory = new SubCategory();

        subCategory.setId(subCategoryDto.getId());
        subCategory.setName(subCategoryDto.getName());

        return subCategory;
    }

    public ProductImage getProductImage(ProductImageDto productImageDto) {
        ProductImage productImage = new ProductImage();

        productImage.setId(productImageDto.getId());
        productImage.setName(productImageDto.getName());

        return productImage;
    }

    @Override
    public List<ProductDto> readAll() {
        List<ProductDto> productDtoList = new ArrayList<>();

        Iterable<Product> products = productRepository.findAll();

        for (Product product : products) {
            ProductDto productDto = getProductDto(product);

            productDtoList.add(productDto);
        }

        return productDtoList;
    }

    public ProductDto getProductDto(Product product) {
        ProductDto productDto = new ProductDto();

        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setPrice(product.getPrice());

        Iterable<Category> categories = categoryRepository.findAll();

        for (Category category : categories) {
            for (SubCategory subCategory : category.getSubCategories()) {
                if (subCategory.getId().equalsIgnoreCase(product.getSubCategory().getId())) {
                    productDto.setCategoryId(category.getId());
                }

            }
        }

        productDto.setSubCategoryId(product.getSubCategory().getId());

        SubCategoryDto subCategoryDto = getSubCategoryDto(product.getSubCategory());

        productDto.setSubCategory(subCategoryDto);

        Set<ProductImageDto> productImagesDto = new HashSet<>();

        Set<ProductImage> productImages = product.getImages();

        for (ProductImage productImage : productImages) {
            ProductImageDto productImageDto = getProductImageDto(productImage);

            productImagesDto.add(productImageDto);
        }

        productDto.setImages(productImagesDto);

        Set<ProductSizeDto> productSizesDto = new HashSet<>();

        Set<ProductSize> productSizes = product.getProductsizes();

        for (ProductSize productSize : productSizes) {
            ProductSizeDto productSizeDto = getProductSizeDto(productSize);

            productSizesDto.add(productSizeDto);
        }

        productDto.setProductsizes(productSizesDto);

        Offer offer = product.getOffer();

        if (offer != null) {
            OfferDto offerDto = getOfferDto(offer);

            productDto.setOffer(offerDto);
        }

        return productDto;
    }

    public SubCategoryDto getSubCategoryDto(SubCategory subCategory) {
        SubCategoryDto subCategoryDto = new SubCategoryDto();

        subCategoryDto.setId(subCategory.getId());
        subCategoryDto.setName(subCategory.getName());

        return subCategoryDto;
    }

    public ProductImageDto getProductImageDto(ProductImage productImage) {
        ProductImageDto productImageDto = new ProductImageDto();

        productImageDto.setId(productImage.getId());
        productImageDto.setName(productImage.getName());

        return productImageDto;
    }

    public ProductSize getProductSize(ProductSizeDto productSizeDto) {
        ProductSize productSize = new ProductSize();

        productSize.setId(productSizeDto.getId());
        productSize.setName(productSizeDto.getName());
        productSize.setQty(productSizeDto.getQty());

        return productSize;
    }

    public ProductSizeDto getProductSizeDto(ProductSize productSize) {
        ProductSizeDto productSizeDto = new ProductSizeDto();

        productSizeDto.setId(productSize.getId());
        productSizeDto.setName(productSize.getName());
        productSizeDto.setQty(productSize.getQty());

        return productSizeDto;
    }

    public OfferDto getOfferDto(Offer offer) {
        OfferDto offerDto = new OfferDto();

        offerDto.setId(offer.getId());
        offerDto.setName(offer.getName());
        offerDto.setPercentage(offer.getPercentage());
        offerDto.setAvailability(offer.isAvailability());

        return offerDto;
    }

    @Override
    public List<ProductDto> findBySubCategoryId(String subCategoryId) {
        List<ProductDto> productDtoList = new ArrayList<>();

        List<Product> products = productRepository.findBySubCategoryId(subCategoryId);

        for (Product product : products) {
            ProductDto productDto = getProductDto(product);

            productDtoList.add(productDto);
        }

        return productDtoList;
    }

    @Override
    public ProductDto findById(String id) {
        return getProductDto(productRepository.findById(id).get());
    }

    @Override
    public boolean update(ProductDto productDto) {
        Product product = productRepository.findById(productDto.getId()).get();

        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setPrice(productDto.getPrice());

        if (productDto.getSubCategoryId() != null) {
            SubCategory subCategory = subCategoryRepository.findById(productDto.getSubCategoryId()).get();
            product.setSubCategory(subCategory);
        } else {
            SubCategory subCategory = getSubCategory(productDto.getSubCategory());
            product.setSubCategory(subCategory);
        }

        Product productO = productRepository.save(product);

        if (productO == null) {
            return false;
        }

        Set<ProductImageDto> productImagesDto = productDto.getImages();
        
        for (ProductImageDto productImageDto : productImagesDto) {
            ProductImage productImage = getProductImage(productImageDto);
            productImage.setProduct(productO);
            productImageRepository.save(productImage);
        }

        Set<ProductSize> productsizes = product.getProductsizes();

        for (ProductSize productsize : productsizes) {
            productSizeRepository.delete(productsize);
        }

        Set<ProductSizeDto> productSizesDto = productDto.getProductsizes();

        for (ProductSizeDto productSizeDto : productSizesDto) {
            ProductSize productSize = getProductSize(productSizeDto);
            productSize.setProduct(productO);
            productSizeRepository.save(productSize);
        }

        OfferDto offerDto = productDto.getOffer();

        if (offerDto != null) {
            Offer offer = new Offer();
            offer.setId(offerDto.getId());
            offer.setName(offerDto.getName());
            offer.setPercentage(offerDto.getPercentage());
            offer.setAvailability(offerDto.isAvailability());
            offer.setProduct(productO);
            offerRepository.save(offer);
        }

        return true;
    }

    @Override
    public void deleteById(String id) {
        productRepository.deleteById(id);
    }

    @Override
    public long count() {
        return productRepository.count();
    }

}
