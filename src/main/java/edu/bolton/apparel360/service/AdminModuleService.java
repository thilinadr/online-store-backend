/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.AdminModuleDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface AdminModuleService {
    public boolean save(AdminModuleDto adminModuleDto);
    public List<AdminModuleDto> readAll();
    public AdminModuleDto findById(String id);
    public boolean update(AdminModuleDto adminModuleDto);
    public void deleteById(String id);
}
