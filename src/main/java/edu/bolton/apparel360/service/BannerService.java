/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.BannerDto;

/**
 *
 * @author Sachin
 */
public interface BannerService {
    public boolean save(BannerDto bannerDto);
    public void deleteById(String id);
}
