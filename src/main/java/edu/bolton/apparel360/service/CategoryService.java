/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.service;

import edu.bolton.apparel360.dto.CategoryDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface CategoryService {
    public boolean save(CategoryDto categoryDto);
    public List<CategoryDto> readAll();
    public CategoryDto findById(String id);
    public boolean update(CategoryDto categoryDto);
    public void deleteById(String id);
}
