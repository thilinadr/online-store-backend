/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import edu.bolton.apparel360.dto.AdminRoleDto;
import edu.bolton.apparel360.dto.AdminUserDto;
import edu.bolton.apparel360.service.AdminRoleService;
import edu.bolton.apparel360.service.AdminUserService;
import edu.bolton.apparel360.service.CustomerService;
import edu.bolton.apparel360.service.PlaceOrderService;
import edu.bolton.apparel360.service.ProductService;
import edu.bolton.apparel360.util.MD5;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class AdminUserController {
    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private AdminUserService adminUserService;
    @Autowired
    private PlaceOrderService placeOrderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CustomerService customerService;
    
    @GetMapping("/")
    public String root() throws Exception {       
        return "login";
    }
    
    @GetMapping("/login")
    public String login() throws Exception {       
        return "login";
    }
            
    @PostMapping("/login")
    public ModelAndView authenticate(HttpSession httpSession,@RequestParam String username,@RequestParam String password){

        ModelAndView mav = new  ModelAndView();

        AdminUserDto user = null;
                
        try{
            user = adminUserService.findByUsernameAndPassword(username, password); 
        }catch(Exception e){
            mav.addObject("login_error","Username or password incorrect");
            mav.setViewName("login");
        }
                      
        if(user!= null){
            httpSession.setAttribute("adminuser", user);
            mav.setViewName("redirect:/dashboard");
        }else{
            mav.addObject("login_error","Username or password incorrect");
            mav.setViewName("login");
        }

        return mav;
    }
    
    @GetMapping("/dashboard")
    public ModelAndView dashboard() throws Exception {
        ModelAndView mav = new ModelAndView("dashboard");
        mav.addObject("title", "Apparel360 | Dashboard");
        mav.addObject("orders", placeOrderService.count());
        mav.addObject("products", productService.count());
        mav.addObject("customers", customerService.count());
        mav.addObject("sales", placeOrderService.sales());
        
        return mav;
    }
            
    @GetMapping("/logout")
    public String logout(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.setAttribute("adminuser", null);
        
        return "login";
    }
    
    @GetMapping("/add_user")
    public ModelAndView viewAddUser() throws Exception {  
        ModelAndView mav = new ModelAndView("administration/user/add_user");
        mav.addObject("title", "Apparel360 | User | Add User");
        mav.addObject("role", adminRoleService.readAll());
        
        return mav;
    }
    
    @PostMapping("/add_user")
    public ModelAndView saveAddUser(@RequestParam String username,@RequestParam String password,@RequestParam String role) throws Exception {
        ModelAndView mav = new  ModelAndView("administration/user/add_user");
        
        AdminUserDto adminUserDto = new AdminUserDto();
        adminUserDto.setUsername(username);
        adminUserDto.setPassword(MD5.encrypt(password));
        
        AdminRoleDto adminRoleDto = adminRoleService.findById(role);
        
        adminUserDto.setAdminRole(adminRoleDto);
                       
        try{
            boolean isSaved = adminUserService.save(adminUserDto);
        
            if(isSaved){
                mav.addObject("msg", "User added successfully");
            }else{
                mav.addObject("errmsg", "User add failed");
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            mav.addObject("errmsg", "User already exists");
        }
      
        return mav;
    }
    
    @GetMapping("/view_user")
    public ModelAndView viewViewUser() throws Exception {  
        ModelAndView mav = new ModelAndView("administration/user/view_user");
        mav.addObject("title", "Apparel360 | User | View User");
        
        return mav;
    }
    
    @GetMapping("/view_user_dt")
    @ResponseBody
    public Map viewUserdt() throws Exception {
        List<AdminUserDto> adminUserDtoList = adminUserService.readAll();
        List entityList=new ArrayList<>();


        for(AdminUserDto adminUserDto:adminUserDtoList){
            List entity=new ArrayList<>();
            
            entity.add(adminUserDto.getUsername());
            entity.add(adminUserDto.getAdminRole().getName());
            entity.add("<a href=\"../update_user/"+adminUserDto.getId()+"\" data-toggle=\"tooltip\" title=\"Edit User\"><i class=\"fas fa-edit\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\""+adminUserDto.getId()+"\" title=\"Delete User\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);

        }

        Map responseMap=new HashMap<>();
        responseMap.put("data",entityList);
        return responseMap;
    }
    
    @GetMapping(value = "/update_user/{id}")
    public ModelAndView viewUpdateUser(@PathVariable String id) throws Exception{
        AdminUserDto adminUserDto = adminUserService.findById(id);
        
        ModelAndView mav = new ModelAndView("administration/user/update_user");
        mav.addObject("title", "Apparel360 | User | Update User");
        mav.addObject("role", adminRoleService.readAll());
        mav.addObject("user", adminUserDto);
        
        return mav;
    }
    
    @PostMapping("/update_user")
    @ResponseBody
    public String updateUser(@RequestParam String id,@RequestParam String username,@RequestParam String role) throws Exception {        
        AdminUserDto adminUserDto = adminUserService.findById(id);
        adminUserDto.setUsername(username);
        
        AdminRoleDto adminRoleDto = adminRoleService.findById(role);
        
        adminUserDto.setAdminRole(adminRoleDto);            
       
        boolean isUpdated = adminUserService.update(adminUserDto);
        
        if(isUpdated){
            return "200";
        }
        
        return "500";
    }
    
    @PostMapping("/delete_user")
    @ResponseBody
    public String deleteUser(@RequestParam String id) throws Exception {    
        adminUserService.deleteById(id);                     
        return "200";
    }
}
