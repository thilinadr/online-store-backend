/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import edu.bolton.apparel360.dto.AdminModuleDto;
import edu.bolton.apparel360.dto.AdminRoleDto;
import edu.bolton.apparel360.service.AdminModuleService;
import edu.bolton.apparel360.service.AdminRoleService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class AdminRoleController {
    @Autowired
    private AdminModuleService adminModuleService;
    @Autowired
    private AdminRoleService adminRoleService;
    
    @GetMapping("/add_role")
    public ModelAndView viewAddRole() throws Exception {  
        ModelAndView mav = new ModelAndView("administration/role/add_role");
        mav.addObject("title", "Apparel360 | Role | Add Role");
        mav.addObject("module", adminModuleService.readAll());
        
        return mav;
    }
    
    @PostMapping("/add_role")
    public ModelAndView saveAddRole(@RequestParam String name,@RequestParam(value = "modules[]") String[] modules) throws Exception {
        ModelAndView mav = new  ModelAndView("administration/role/add_role");
        
        AdminRoleDto adminRoleDto = new AdminRoleDto();  
        adminRoleDto.setName(name);
        
        Set<AdminModuleDto> adminModuleDtos = new HashSet<>();
        
        for (String moduleId : modules) {
            AdminModuleDto adminModuleDto = adminModuleService.findById(moduleId);
            
            adminModuleDtos.add(adminModuleDto);
        }
        
        adminRoleDto.setModules(adminModuleDtos);
        
        try{
            boolean isSaved = adminRoleService.save(adminRoleDto);
        
            if(isSaved){
                mav.addObject("msg", "Role added successfully");
            }else{
                mav.addObject("errmsg", "Role add failed");
            }
        }catch(Exception e){
            mav.addObject("errmsg", "Role already exists");
        }
      
        return mav;
    }
    
    @GetMapping("/view_role")
    public ModelAndView viewViewRole() throws Exception {  
        ModelAndView mav = new ModelAndView("administration/role/view_role");
        mav.addObject("title", "Apparel360 | Role | View Role");
        
        return mav;
    }
    
    @GetMapping("/view_role_dt")
    @ResponseBody
    public Map viewRoledt() throws Exception {
        List<AdminRoleDto> adminRoleDtoList = adminRoleService.readAll();
        List entityList=new ArrayList<>();


        for(AdminRoleDto adminRoleDto:adminRoleDtoList){
            List entity=new ArrayList<>();
            
            entity.add(adminRoleDto.getName());
            entity.add("<a type=\"button\" data-toggle=\"modal\" data-target=\"#roleModulesModal\" data-toggle=\"tooltip\" title=\"Role Menu\"><i class=\"fas fa-th-list\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"../update_role/"+adminRoleDto.getId()+"\" data-toggle=\"tooltip\" title=\"Edit Role\"><i class=\"fas fa-edit\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\""+adminRoleDto.getId()+"\" title=\"Delete Role\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);
        }

        Map responseMap=new HashMap<>();
        responseMap.put("data",entityList);
        return responseMap;
    }
    
    @GetMapping(value = "/update_role/{id}")
    public ModelAndView viewUpdateRole(@PathVariable String id) throws Exception{
        AdminRoleDto adminRoleDto = adminRoleService.findById(id);
        
        ModelAndView mav = new ModelAndView("administration/role/update_role");
        mav.addObject("title", "Apparel360 | Role | Update Role");
        mav.addObject("module", adminModuleService.readAll());
        mav.addObject("role", adminRoleDto);
        
        return mav;
    }
    
    @PostMapping("/update_role")
    @ResponseBody
    public String updateRole(@RequestParam String id,@RequestParam String name,@RequestParam(value = "modules[]") String[] modules){
        AdminRoleDto adminRoleDto = new AdminRoleDto();  
        adminRoleDto.setId(id);
        adminRoleDto.setName(name);
        
        Set<AdminModuleDto> adminModuleDtos = new HashSet<>();
        
        for (String moduleId : modules) {
            AdminModuleDto adminModuleDto = adminModuleService.findById(moduleId);
            
            adminModuleDtos.add(adminModuleDto);
        }
        
        adminRoleDto.setModules(adminModuleDtos);
        
        boolean isUpdated = adminRoleService.update(adminRoleDto);
        
        if(isUpdated){
            return "200";
        }
        
        return "500";
    }
    
    @PostMapping("/delete_role")
    @ResponseBody
    public String deleteRole(@RequestParam String id) throws Exception {    
        adminRoleService.deleteById(id);                     
        return "200";
    }
    
    @GetMapping("/role_dt/{id}")
    @ResponseBody
    public AdminRoleDto roleDt(@PathVariable String id) throws Exception {
        return adminRoleService.findById(id);
    }
}
