/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import edu.bolton.apparel360.dto.AdminModuleDto;
import edu.bolton.apparel360.service.AdminModuleService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class AdminModuleController {

    @Autowired
    private AdminModuleService adminModuleService;

    @GetMapping("/add_module")
    public ModelAndView viewAddModule() throws Exception {
        ModelAndView mav = new ModelAndView("administration/module/add_module");
        mav.addObject("title", "Apparel360 | Module | Add Module");
        
        return mav;
    }

    @PostMapping("/add_module")
    public ModelAndView saveAddModule(@RequestParam String name) {
        ModelAndView mav = new ModelAndView("administration/module/add_module");

        AdminModuleDto adminModuleDto = new AdminModuleDto();
        adminModuleDto.setName(name);

        try {
            boolean isSaved = adminModuleService.save(adminModuleDto);

            if (isSaved) {
                mav.addObject("msg", "Module added successfully");
            } else {
                mav.addObject("errmsg", "Module add failed");
            }
        } catch (Exception e) {
            mav.addObject("errmsg", "Module already exists");
        }

        return mav;
    }

    @GetMapping("/view_module")
    public ModelAndView viewViewModule() throws Exception {
        ModelAndView mav = new ModelAndView("administration/module/view_module");
        mav.addObject("title", "Apparel360 | Module | View Module");
        
        return mav;
    }

    @GetMapping("/view_module_dt")
    @ResponseBody
    public Map viewModuledt() throws Exception {
        List<AdminModuleDto> adminModuleDtoList = adminModuleService.readAll();
        List entityList = new ArrayList<>();

        for (AdminModuleDto adminModuleDto : adminModuleDtoList) {
            List entity = new ArrayList<>();

            entity.add(adminModuleDto.getName());
            entity.add("<a href=\"../update_module/" + adminModuleDto.getId() + "\" data-toggle=\"tooltip\" title=\"Edit Module\"><i class=\"fas fa-edit\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\"" + adminModuleDto.getId() + "\" title=\"Delete Module\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);

        }

        Map responseMap = new HashMap<>();
        responseMap.put("data", entityList);
        return responseMap;
    }

    @GetMapping(value = "/update_module/{id}")
    public ModelAndView viewUpdateModule(@PathVariable String id) throws Exception {
        AdminModuleDto adminModuleDto = adminModuleService.findById(id);

        ModelAndView mav = new ModelAndView("administration/module/update_module");
        mav.addObject("title", "Apparel360 | Module | Update Module");
        mav.addObject("module", adminModuleDto);

        return mav;
    }

    @PostMapping("/update_module")
    @ResponseBody
    public String updateModule(@RequestBody AdminModuleDto adminModuleDto) {
        boolean isUpdated = adminModuleService.update(adminModuleDto);

        if (isUpdated) {
            return "200";
        }

        return "500";
    }

    @PostMapping("/delete_module")
    @ResponseBody
    public String deleteModule(@RequestParam String id) throws Exception {
        adminModuleService.deleteById(id);
        return "200";
    }
}
