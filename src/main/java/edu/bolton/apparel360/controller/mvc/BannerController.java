/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import edu.bolton.apparel360.dto.BannerDto;
import edu.bolton.apparel360.service.BannerService;
import edu.bolton.apparel360.service.CompanyService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class BannerController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private BannerService bannerService;
    @Value("${upload.path}")
    String UPLOAD_PATH;
    
    @GetMapping("/upload_banner")
    public ModelAndView viewUploadBanner() throws Exception {
        ModelAndView mav = new ModelAndView("administration/banner/upload_banner");
        mav.addObject("title", "Apparel360 | Administration | Banner | Upload Banner");

        return mav;
    }
    
    @PostMapping("/upload_banner")
    public ModelAndView saveUploadBanner(@RequestParam MultipartFile banner,@RequestParam String description,@RequestParam String url) throws IOException{
        ModelAndView mav = new ModelAndView("administration/banner/upload_banner");
        
        byte[] bytes = banner.getBytes();
        Path path = Paths.get(UPLOAD_PATH + banner.getOriginalFilename().replaceAll("\\s", ""));
        Files.write(path, bytes);
        
        BannerDto bannerDto = new BannerDto();
        
        bannerDto.setId(UUID.randomUUID().toString());
        bannerDto.setImage(banner.getOriginalFilename().replaceAll("\\s", ""));
        bannerDto.setDescription(description);
        bannerDto.setUrl(url);

        try {
            boolean isSaved = bannerService.save(bannerDto);

            if (isSaved) {
                mav.addObject("msg", "Banner uploaded successfully");
            } else {
                mav.addObject("errmsg", "Banner upload failed");
            }
        } catch (Exception e) {
            mav.addObject("errmsg", "Banner upload failed");
        }

        return mav;
    }
    
    @GetMapping("/view_banner")
    public ModelAndView viewViewBanner() throws Exception {
        ModelAndView mav = new ModelAndView("administration/banner/view_banner");
        mav.addObject("title", "Apparel360 | Administration | Banner | View Banner");

        return mav;
    }
    
    @GetMapping("/view_banner_dt")
    @ResponseBody
    public Map viewBannerdt() throws Exception {
        Set<BannerDto> banners = companyService.findById("1").getBanners();
        List entityList = new ArrayList<>();

        for (BannerDto bannerDto : banners) {
            List entity = new ArrayList<>();

            entity.add(bannerDto.getImage());
            entity.add(bannerDto.getDescription());
            entity.add(bannerDto.getUrl());
            entity.add("<a href=\"javascript:void(0)\" data-id=\"" + bannerDto.getId() + "\" title=\"Delete Banner\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);

        }

        Map responseMap = new HashMap<>();
        responseMap.put("data", entityList);
        return responseMap;
    }
    
    @PostMapping("/delete_banner")
    @ResponseBody
    public String deleteBanner(@RequestParam String id) throws Exception {    
        bannerService.deleteById(id);                     
        return "200";
    }
}
