/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import com.google.gson.Gson;
import edu.bolton.apparel360.dto.OfferDto;
import edu.bolton.apparel360.dto.ProductDto;
import edu.bolton.apparel360.dto.ProductImageDto;
import edu.bolton.apparel360.dto.ProductSizeDto;
import edu.bolton.apparel360.service.OfferService;
import edu.bolton.apparel360.service.ProductService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.activation.FileTypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private OfferService offerService;
    @Value("${upload.path}")
    String UPLOAD_PATH;

    @GetMapping("/add_product")
    public ModelAndView viewAddProduct() throws Exception {
        ModelAndView mav = new ModelAndView("product/add_product");
        mav.addObject("title", "Apparel360 | Product | Add Product");

        return mav;
    }

    @PostMapping("/add_product")
    @ResponseBody
    public String saveAddProduct(@RequestParam(value = "category") String categoryId, @RequestParam(value = "sub_category") String subCategoryId, @RequestParam String name, @RequestParam String description, @RequestParam double price, @RequestParam(value = "image") MultipartFile[] images, @RequestParam String items) throws IOException {

        ProductDto productDto = new ProductDto();

        productDto.setSubCategoryId(subCategoryId);
        productDto.setName(name);
        productDto.setDescription(description);
        productDto.setPrice(price);

        Set<ProductImageDto> productImageDtoList = new HashSet<>();

        for (MultipartFile image : images) {

            byte[] bytes = image.getBytes();
            Path path = Paths.get(UPLOAD_PATH + image.getOriginalFilename().replaceAll("\\s", ""));
            Files.write(path, bytes);

            ProductImageDto productImageDto = new ProductImageDto();

            productImageDto.setId(UUID.randomUUID().toString());
            productImageDto.setName(image.getOriginalFilename().replaceAll("\\s", ""));

            productImageDtoList.add(productImageDto);
        }

        productDto.setImages(productImageDtoList);

        Set<ProductSizeDto> productSizeDtoList = new HashSet<>();

        Gson gson = new Gson();

        ProductSizeDto[] productSizeDtos = gson.fromJson(items, ProductSizeDto[].class);

        for (ProductSizeDto productSizeDto : productSizeDtos) {
            productSizeDto.setId(UUID.randomUUID().toString());

            productSizeDtoList.add(productSizeDto);
        }

        productDto.setProductsizes(productSizeDtoList);

        try {
            boolean isSaved = productService.save(productDto);

            if (isSaved) {
                return "200";
            } else {
                return "500";
            }
        } catch (Exception e) {
            return "304";
        }

    }

    @GetMapping("/view_product")
    public ModelAndView viewViewProduct() throws Exception {
        ModelAndView mav = new ModelAndView("product/view_product");
        mav.addObject("title", "Apparel360 | Product | View Product");

        return mav;
    }

    @GetMapping("/view_product_dt")
    @ResponseBody
    public Map viewProductdt() throws Exception {
        List<ProductDto> productDtoList = productService.readAll();
        List entityList = new ArrayList<>();

        for (ProductDto productDto : productDtoList) {
            List entity = new ArrayList<>();

            entity.add(productDto.getSubCategory().getName());
            entity.add(productDto.getName());
            entity.add(productDto.getPrice());
            entity.add(productDto.getRating());
            entity.add("<a type=\"button\" style=\"font-size: 21px; margin-top: -2px;\" class=\"mdi mdi-alpha-o-box icon-size\" data-toggle=\"modal\" data-target=\"#offerModal\" title=\"Offers\"></a>");
            entity.add("<a href=\"../update_product/" + productDto.getId() + "\" data-toggle=\"tooltip\" title=\"Edit Product\"><i class=\"fas fa-edit\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\"" + productDto.getId() + "\" title=\"Delete Product\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);

        }

        Map responseMap = new HashMap<>();
        responseMap.put("data", entityList);
        return responseMap;
    }
    
    @GetMapping(value = "/update_product/{id}")
    public ModelAndView viewUpdateProduct(@PathVariable String id) throws Exception {
        ProductDto productDto = productService.findById(id);

        ModelAndView mav = new ModelAndView("product/update_product");
        mav.addObject("product", productDto);

        return mav;
    }
    
    @PostMapping("/update_product")
    @ResponseBody
    public String updateProduct(@RequestParam String id,@RequestParam(value = "category") String categoryId, @RequestParam(value = "sub_category") String subCategoryId, @RequestParam String name, @RequestParam String description, @RequestParam double price, @RequestParam(value = "image",required = false) MultipartFile[] images, @RequestParam String items) throws IOException {
        ProductDto productDto = new ProductDto();
        
        productDto.setId(id);
        productDto.setSubCategoryId(subCategoryId);
        productDto.setName(name);
        productDto.setDescription(description);
        productDto.setPrice(price);

        Set<ProductImageDto> productImageDtoList = new HashSet<>();
        
        if(images.length != 0){
            for (MultipartFile image : images) {
                if(!image.isEmpty()){
                    byte[] bytes = image.getBytes();
                    Path path = Paths.get(UPLOAD_PATH + image.getOriginalFilename().replaceAll("\\s", ""));
                    Files.write(path, bytes);

                    ProductImageDto productImageDto = new ProductImageDto();

                    productImageDto.setId(UUID.randomUUID().toString());
                    productImageDto.setName(image.getOriginalFilename().replaceAll("\\s", ""));

                    productImageDtoList.add(productImageDto);
                }
            }

            productDto.setImages(productImageDtoList);
        }
        
        Set<ProductSizeDto> productSizeDtoList = new HashSet<>();

        Gson gson = new Gson();

        ProductSizeDto[] productSizeDtos = gson.fromJson(items, ProductSizeDto[].class);

        for (ProductSizeDto productSizeDto : productSizeDtos) {
            if(productSizeDto.getId().isEmpty()){
                productSizeDto.setId(UUID.randomUUID().toString());
            }

            productSizeDtoList.add(productSizeDto);
        }

        productDto.setProductsizes(productSizeDtoList);

        try {
            boolean isSaved = productService.update(productDto);

            if (isSaved) {
                return "200";
            } else {
                return "500";
            }
        } catch (Exception e) {
            return "304";
        }

    }

    @PostMapping("/add_offer")
    @ResponseBody
    public String saveAddOffer(@RequestParam(value = "product_id") String productId,@RequestParam String id, @RequestParam String name, @RequestParam int percentage, @RequestParam(required = false) boolean availability) throws IOException {
        ProductDto productDto = productService.findById(productId);
        
        OfferDto offerDto = new OfferDto();
        
        if(id.isEmpty()){
            offerDto.setId(UUID.randomUUID().toString());
        }else{
            offerDto.setId(id);
        }
        
        offerDto.setName(name);
        offerDto.setPercentage(percentage);
        offerDto.setAvailability(availability);

        productDto.setOffer(offerDto);

        boolean isUpdated = productService.update(productDto);

        if (isUpdated) {
            return "200";
        } else {
            return "500";
        }
    }
    
    @GetMapping("/product_offer/{id}")
    @ResponseBody
    public OfferDto getOfferByProductId(@PathVariable String id){
        return offerService.findByProductId(id);     
    }
    
    @PostMapping("/delete_product")
    @ResponseBody
    public String deleteProduct(@RequestParam String id) throws Exception {    
        productService.deleteById(id);                     
        return "200";
    }
    
    @RequestMapping(value = "/uploads/{name:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable String name) throws IOException {
        File img = new File(UPLOAD_PATH + name);
        return ResponseEntity.ok().contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img))).body(Files.readAllBytes(img.toPath()));
    }
    
}
