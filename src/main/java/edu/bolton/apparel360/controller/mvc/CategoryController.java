/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.mvc;

import edu.bolton.apparel360.dto.CategoryDto;
import edu.bolton.apparel360.dto.SubCategoryDto;
import edu.bolton.apparel360.service.CategoryService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sachin
 */
@Controller
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    
    @GetMapping("/add_category")
    public ModelAndView viewAddCategory() throws Exception {
        ModelAndView mav = new ModelAndView("category/add_category");
        mav.addObject("title", "Apparel360 | Category | Add Category");

        return mav;
    }

    @PostMapping("/add_category")
    @ResponseBody
    public String saveCategory(HttpSession httpSession, @RequestParam String name, @RequestParam(value = "items[]", required = false) String[] items) {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(name);

        if (items.length != 0) {
            Set<SubCategoryDto> subCategoryDtos = new HashSet<>();

            for (String subCategory : items) {
                SubCategoryDto subCategoryDto = new SubCategoryDto();

                subCategoryDto.setId(UUID.randomUUID().toString());
                subCategoryDto.setName(subCategory);

                subCategoryDtos.add(subCategoryDto);
            }

            categoryDto.setSubCategories(subCategoryDtos);
        }

        try {
            boolean isSaved = categoryService.save(categoryDto);

            if (isSaved) {
                return "200";
            } else {
                return "500";
            }

        } catch (Exception e) {
            return "304";
        }
    }

    @GetMapping("/view_category")
    public ModelAndView viewViewCategory() throws Exception {
        ModelAndView mav = new ModelAndView("category/view_category");
        mav.addObject("title", "Apparel360 | Category | View Category");

        return mav;
    }

    @GetMapping("/view_category_dt")
    @ResponseBody
    public Map viewCategorydt() throws Exception {
        List<CategoryDto> categoryDtoList = categoryService.readAll();
        List entityList = new ArrayList<>();

        for (CategoryDto categoryDto : categoryDtoList) {
            List entity = new ArrayList<>();

            entity.add(categoryDto.getName());
            entity.add("<a href=\"../update_category/" + categoryDto.getId() + "\" data-toggle=\"tooltip\" title=\"Edit Category\"><i class=\"fas fa-edit\" style=\"font-size:16px;\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\"" + categoryDto.getId() + "\" title=\"Delete Category\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);

        }

        Map responseMap = new HashMap<>();
        responseMap.put("data", entityList);
        return responseMap;
    }

    @GetMapping(value = "/update_category/{id}")
    public ModelAndView viewUpdateCategory(@PathVariable String id) throws Exception {
        CategoryDto categoryDto = categoryService.findById(id);

        ModelAndView mav = new ModelAndView("category/update_category");
        mav.addObject("category", categoryDto);

        return mav;
    }

    @PostMapping("/update_category")
    @ResponseBody
    public String updateCategory(HttpSession httpSession, @RequestParam String id, @RequestParam String name, @RequestParam(value = "items[]", required = false) String[] items) {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(id);
        categoryDto.setName(name);

        if (items.length != 0) {
            Set<SubCategoryDto> subCategoryDtos = new HashSet<>();

            for (String subCategory : items) {
                SubCategoryDto subCategoryDto = new SubCategoryDto();

                subCategoryDto.setId(UUID.randomUUID().toString());
                subCategoryDto.setName(subCategory);

                subCategoryDtos.add(subCategoryDto);
            }

            categoryDto.setSubCategories(subCategoryDtos);
        }

        boolean isUpdated = categoryService.update(categoryDto);

        if (isUpdated) {
            return "200";
        } else {
            return "500";
        }

    }
    
    @PostMapping("/delete_category")
    @ResponseBody
    public String deleteCategory(@RequestParam String id) throws Exception {    
        categoryService.deleteById(id);                     
        return "200";
    }
    
    @GetMapping("/category_dt")
    @ResponseBody
    public List<CategoryDto> categoryDt() throws Exception {    
        return categoryService.readAll();
    }
    
    @GetMapping("/sub_category_dt/{id}")
    @ResponseBody
    public Set<SubCategoryDto> subCategoryDt(@PathVariable String id) throws Exception {   
        return categoryService.findById(id).getSubCategories();
    }
}
