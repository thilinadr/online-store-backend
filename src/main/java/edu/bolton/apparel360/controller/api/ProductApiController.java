/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.api;

import edu.bolton.apparel360.service.OfferService;
import edu.bolton.apparel360.service.ProductService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.activation.FileTypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sachin
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class ProductApiController {
    @Autowired
    private ProductService productService;
    @Autowired
    private OfferService offerService;
    @Value("${upload.path}")
    String UPLOAD_PATH;
    
    @RequestMapping(value = "/uploads/{name:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable String name) throws IOException {
        File img = new File(UPLOAD_PATH + name);
        return ResponseEntity.ok().contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img))).body(Files.readAllBytes(img.toPath()));
    }
    
    @GetMapping("/product_category/{id}")
    public ResponseEntity findBySubCategoryId(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(productService.findBySubCategoryId(id));
    }   
    
    @GetMapping("/product/{id}")
    public ResponseEntity findById(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(productService.findById(id));
    }

    @GetMapping("/product")
    public ResponseEntity getAllProducts() throws Exception {
        return ResponseEntity.ok(productService.readAll());
    }
    
    @GetMapping("/offers")
    public ResponseEntity getAllOffers() throws Exception {
        return ResponseEntity.ok(offerService.readAll());
    }
}
