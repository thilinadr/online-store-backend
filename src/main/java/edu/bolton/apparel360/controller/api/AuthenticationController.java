/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.api;

import edu.bolton.apparel360.config.JwtTokenUtil;
import edu.bolton.apparel360.dto.CustomerDto;
import edu.bolton.apparel360.model.Customer;
import edu.bolton.apparel360.service.impl.CustomerDetailsServiceImpl;
import edu.bolton.apparel360.util.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sachin
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private CustomerDetailsServiceImpl customerDetailsService;

    @PostMapping("/token")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody CustomerDto userDto) throws Exception {
        
        authenticate(userDto.getUsername(), userDto.getPassword());

        final UserDetails userDetails = customerDetailsService.loadUserByUsername(userDto.getUsername());

        final CustomerDto customer = customerDetailsService.findCustomer(userDto.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token,customer));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
