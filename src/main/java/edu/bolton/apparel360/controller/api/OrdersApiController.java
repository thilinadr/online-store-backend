package edu.bolton.apparel360.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bolton.apparel360.dto.OrdersDto;
import edu.bolton.apparel360.response.SuccessResponseHandler;
import edu.bolton.apparel360.service.PlaceOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author thisu96
 * @Date 20/07/2020
 * @Time 16:39
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class OrdersApiController {
    @Autowired
    private PlaceOrderService placeOrderService;
    private ObjectMapper mapper = new ObjectMapper();

    @PostMapping("/add/order")
    public ResponseEntity makeOrder(@RequestBody String ordersJsonString) throws IOException {
        OrdersDto ordersDto = mapper.readValue(ordersJsonString, OrdersDto.class);
        boolean placeOrder = placeOrderService.placeOrder(ordersDto);
        return SuccessResponseHandler.generateResponse(placeOrder);
    }
    
    @GetMapping("customerId/{id}/order")
    public ResponseEntity getOrdersByCustomerId(@PathVariable long id) throws IOException {
        return ResponseEntity.ok(placeOrderService.readOrdersByCustomerId(id));
    }
}
