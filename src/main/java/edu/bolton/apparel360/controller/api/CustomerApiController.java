package edu.bolton.apparel360.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bolton.apparel360.response.SuccessResponseHandler;
import edu.bolton.apparel360.dto.CustomerDto;
import edu.bolton.apparel360.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 10:57
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class CustomerApiController {

    @Autowired
    private CustomerService customerService;

    private ObjectMapper mapper = new ObjectMapper();

    @PostMapping ("/add/customer")
    public ResponseEntity registerCustomer(@RequestBody String customerJsonString) throws IOException {
        CustomerDto customerDto = mapper.readValue(customerJsonString, CustomerDto.class);
        CustomerDto data = customerService.save(customerDto);
        return SuccessResponseHandler.generateResponse(data);
    }


    @GetMapping("/customer/{id}")
    public ResponseEntity findBySubCategoryId(@PathVariable Long id) throws Exception {
        CustomerDto data = customerService.findById(id);
        return SuccessResponseHandler.generateResponse(data);
    }

    @GetMapping("/customers")
    public ResponseEntity findById() throws Exception {
        List<CustomerDto> data = customerService.readAll();
        return SuccessResponseHandler.generateResponse(data);
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity findById(@PathVariable Long id) throws Exception {
        Long data = customerService.deleteById(id);
        return SuccessResponseHandler.generateResponse(data);
    }

    @PutMapping(value="/customer/{id}")
    public ResponseEntity updateCustomer(@PathVariable Long id,@RequestBody CustomerDto dto) throws Exception{
        Long data = customerService.update(id,dto);
        return SuccessResponseHandler.generateResponse(data);
    }

}
