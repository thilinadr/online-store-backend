/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.controller.api;

import edu.bolton.apparel360.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sachin
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class CategoryApiController {
    @Autowired
    private CategoryService categoryService;
    
    @GetMapping("/category")
    public ResponseEntity readAll() throws Exception {
        return ResponseEntity.ok(categoryService.readAll());
    }
}
