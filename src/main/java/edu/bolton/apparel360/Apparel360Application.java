package edu.bolton.apparel360;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apparel360Application {

    public static void main(String[] args) {
        SpringApplication.run(Apparel360Application.class, args);
    }

}
