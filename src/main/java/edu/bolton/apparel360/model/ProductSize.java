/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Sachin
 */
@Entity
@Table(name = "product_size")
public class ProductSize {
    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;
    @Column(nullable = false)
    private String name;
    @Column(columnDefinition = "integer default 0",nullable = false)
    private int qty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "ProductSize{" + "id=" + id + ", product=" + product + ", name=" + name + ", qty=" + qty + '}';
    }

}
