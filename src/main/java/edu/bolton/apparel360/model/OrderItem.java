package edu.bolton.apparel360.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 09:39
 */
@Entity
@Table(name = "order_item")
public class OrderItem implements Serializable{
    @Basic
    @Column(name = "qty")
    private long qty;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="item_id",referencedColumnName = "item_id",insertable = false,updatable = false)
    private Product product;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="order_id",referencedColumnName = "order_id",insertable = false, updatable = false)
    private Orders orders;
    @EmbeddedId
    private OrderDetail_PK orderDetail_pk;

    public OrderItem() {
    }

    public OrderItem(int qty,Product product,Orders orders,OrderDetail_PK orderDetail_pk) {
        this.qty = qty;
        this.product = product;
        this.orders = orders;
        this.orderDetail_pk = orderDetail_pk;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public OrderDetail_PK getOrderDetail_pk() {
        return orderDetail_pk;
    }

    public void setOrderDetail_pk(OrderDetail_PK orderDetail_pk) {
        this.orderDetail_pk = orderDetail_pk;
    }

}
