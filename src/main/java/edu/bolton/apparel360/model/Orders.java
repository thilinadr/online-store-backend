package edu.bolton.apparel360.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 09:39
 */
@Entity
@Table(name = "orders")
public class Orders {
    private long order_id;
    private Date orderDate;
    private String streetAddress1;
    private Customer customer;

    public Orders() {
    }

    public Orders(Date orderDate, String streetAddress1,Customer customer) {
        this.orderDate = orderDate;
        this.streetAddress1 = streetAddress1;
        this.customer = customer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long id) {
        this.order_id = id;
    }

    @Basic
    @Column(name = "order_date")
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "street_address1")
    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
