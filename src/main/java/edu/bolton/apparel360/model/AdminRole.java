/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Sachin
 */
@Entity
@Table(name = "admin_role")
public class AdminRole {
    @Id
    private String id;
    @Column(unique = true, nullable = false)
    private String name;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AdminModule> modules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AdminModule> getModules() {
        return modules;
    }

    public void setModules(Set<AdminModule> modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "AdminRole{" + "id=" + id + ", name=" + name + ", modules=" + modules + '}';
    }
    
    
}
