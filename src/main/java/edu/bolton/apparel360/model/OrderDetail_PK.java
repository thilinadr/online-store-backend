package edu.bolton.apparel360.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OrderDetail_PK implements Serializable {
    private String item_id ;
    private Long order_id;

    public OrderDetail_PK() {
    }

    public OrderDetail_PK(String item_id, Long order_id) {
        this.item_id = item_id;
        this.order_id = order_id;
    }

    public String getId() {
        return item_id;
    }

    public void setId(String id) {
        this.item_id = id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }
}
