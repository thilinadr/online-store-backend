/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.model;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Sachin
 */
@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "item_id",nullable = false)
    private String item_id;
    @Column(unique = true, nullable = false)
    private String name;
    @Column(nullable = true)
    private String description;
    @Column(columnDefinition = "decimal default 0",nullable = false)
    private double rating;
    @Column(nullable = false)
    private double price;
    @ManyToOne
    @JoinColumn(name="sub_category_id", nullable=false)
    private SubCategory subCategory;
    @OneToOne(mappedBy = "product")
    private Offer offer;
    @OneToMany(mappedBy="product")
    private Set<ProductImage> images;
    @OneToMany(mappedBy="product")
    private Set<ProductSize> productsizes;

    public String getId() {
        return item_id;
    }

    public void setId(String item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Set<ProductImage> getImages() {
        return images;
    }

    public void setImages(Set<ProductImage> images) {
        this.images = images;
    }

    public Set<ProductSize> getProductsizes() {
        return productsizes;
    }

    public void setProductsizes(Set<ProductSize> productsizes) {
        this.productsizes = productsizes;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + item_id + ", name=" + name + ", description=" + description + ", rating=" + rating + ", price=" + price + ", subCategory=" + subCategory + ", offer=" + offer + ", images=" + images + ", productsizes=" + productsizes + '}';
    }
    
}
