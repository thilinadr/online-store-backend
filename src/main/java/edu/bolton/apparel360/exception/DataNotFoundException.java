package edu.bolton.apparel360.exception;

import edu.bolton.apparel360.response.ResponseCode;
import org.springframework.http.HttpStatus;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:08
 */
public class DataNotFoundException extends MainException {
    private static String message = "Missing Data";
    private static HttpStatus status = HttpStatus.BAD_REQUEST;
    private static Integer code = ResponseCode.MISSING_DATA;

    public DataNotFoundException(String message, String description) {
        super(message, code, status, description);
    }

    public DataNotFoundException(String description) {
        super(message, code, status, description);
    }
}
