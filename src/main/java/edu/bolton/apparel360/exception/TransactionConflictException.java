package edu.bolton.apparel360.exception;

import edu.bolton.apparel360.response.ResponseCode;
import org.springframework.http.HttpStatus;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:18
 */
public class TransactionConflictException extends MainException{

    private static String message = "Conflict";
    private static HttpStatus status = HttpStatus.CONFLICT;
    private static Integer code = ResponseCode.CONFLICT_DATA_VALUE;

    public TransactionConflictException(String description) {
        super(message, code, status, description);
    }

    public TransactionConflictException(String message, String description) {
        super(message, code, status, description);
    }
}
