package edu.bolton.apparel360.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:13
 */
public class MainException extends RuntimeException {
    private String message;
    private Integer code;
    @JsonIgnore
    private HttpStatus status;
    private String description;

    public MainException() {
    }

    public MainException(String message, Integer code, HttpStatus status, String description) {
        this.message = message;
        this.code = code;
        this.status = status;
        this.description = description;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
