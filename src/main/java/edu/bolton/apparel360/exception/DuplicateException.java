package edu.bolton.apparel360.exception;

import edu.bolton.apparel360.response.ResponseCode;
import org.springframework.http.HttpStatus;

/**
 * Exception class for transaction related exception (eg: duplicate, concurrency, etc..)
 *
 /**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:13
 */
public class DuplicateException extends MainException{

    private static String message = "Duplicate";
    private static HttpStatus status = HttpStatus.BAD_REQUEST;
    private static Integer code = ResponseCode.DUPLICATE_DATA;

    public DuplicateException(String description) {
        super(message, code, status, description);
    }

    public DuplicateException(String message, String description) {
        super(message, code, status, description);
    }
}
