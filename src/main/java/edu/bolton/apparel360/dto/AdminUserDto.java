/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.dto;

/**
 *
 * @author Sachin
 */
public class AdminUserDto {
    private String id;
    private String username;
    private String password;
    private AdminRoleDto adminRole;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public AdminRoleDto getAdminRole() {
        return adminRole;
    }

    public void setAdminRole(AdminRoleDto adminRole) {
        this.adminRole = adminRole;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
