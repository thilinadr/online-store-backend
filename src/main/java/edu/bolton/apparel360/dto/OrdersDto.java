package edu.bolton.apparel360.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.bolton.apparel360.model.Payment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import lombok.ToString;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 10:17
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
public class OrdersDto {
    private long id;
    private Date orderDate;
    private Double amount;
    private String streetAddress1;
    private String streetAddress2;
    private String city;
    private String postalCode;
    private CustomerDto customer;
    private List<OrderItemDto> orderDetails;
    private PaymentDto payment;
}
