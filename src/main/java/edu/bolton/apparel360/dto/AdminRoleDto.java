/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.dto;

import java.util.Set;

/**
 *
 * @author Sachin
 */
public class AdminRoleDto {
    private String id;
    private String name;
    private Set<AdminModuleDto> modules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AdminModuleDto> getModules() {
        return modules;
    }

    public void setModules(Set<AdminModuleDto> modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "AdminRole{" + "id=" + id + ", name=" + name + ", modules=" + modules + '}';
    }
    
    
}
