/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.bolton.apparel360.model.Customer;
import edu.bolton.apparel360.model.OrderItem;
import edu.bolton.apparel360.model.Payment;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Sachin
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
public class OrderDto {
    private long id;
    private Date orderDate;
    private String streetAddress1;
    private Customer customer;
    private List<OrderItem> orderDetails;
    private Payment payment;
}
