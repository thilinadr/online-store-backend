/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.dto;

import java.util.Set;

/**
 *
 * @author Sachin
 */
public class CategoryDto {
    private String id;
    private String name;
    private Set<SubCategoryDto> subCategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SubCategoryDto> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Set<SubCategoryDto> subCategories) {
        this.subCategories = subCategories;
    }

    @Override
    public String toString() {
        return "CategoryDto{" + "id=" + id + ", name=" + name + ", subCategories=" + subCategories + '}';
    }
      
    
}
