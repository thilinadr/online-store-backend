package edu.bolton.apparel360.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 10:17
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
    private long id;
    private String name;
    private String username;
    private String password;
    private String address;
    private String contact;
    private String email;
}
