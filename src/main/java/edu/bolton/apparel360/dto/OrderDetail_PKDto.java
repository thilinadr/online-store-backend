package edu.bolton.apparel360.dto;

public class OrderDetail_PKDto {
    private int itemCode;
    private int orderId;

    public OrderDetail_PKDto() {
    }

    public OrderDetail_PKDto(int itemCode, int orderId) {
        this.itemCode = itemCode;
        this.orderId = orderId;
    }
}
