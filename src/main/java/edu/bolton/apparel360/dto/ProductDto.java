/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.dto;

import java.util.Set;

/**
 *
 * @author Sachin
 */
public class ProductDto {
    private String id;
    private String categoryId;
    private String subCategoryId;
    private SubCategoryDto subCategory;
    private String name;
    private String description;
    private double rating;
    private double price;
    private OfferDto offer;
    private Set<ProductImageDto> images;
    private Set<ProductSizeDto> productsizes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
    
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public OfferDto getOffer() {
        return offer;
    }

    public void setOffer(OfferDto offer) {
        this.offer = offer;
    }

    public Set<ProductImageDto> getImages() {
        return images;
    }

    public void setImages(Set<ProductImageDto> images) {
        this.images = images;
    }

    public Set<ProductSizeDto> getProductsizes() {
        return productsizes;
    }

    public void setProductsizes(Set<ProductSizeDto> productsizes) {
        this.productsizes = productsizes;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    @Override
    public String toString() {
        return "ProductDto{" + "id=" + id + ", subCategoryId=" + subCategoryId + ", name=" + name + ", description=" + description + ", rating=" + rating + ", price=" + price + ", offer=" + offer + ", images=" + images + ", productsizes=" + productsizes + '}';
    }

    public SubCategoryDto getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategoryDto subCategory) {
        this.subCategory = subCategory;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    
}
