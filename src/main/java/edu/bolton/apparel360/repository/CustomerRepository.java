/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.repository;

import edu.bolton.apparel360.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sachin
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
    Customer findByUsername(String username);
}
