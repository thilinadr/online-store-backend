/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.repository;

import edu.bolton.apparel360.model.AdminUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sachin
 */
@Repository
public interface AdminUserRepository extends CrudRepository<AdminUser, String>{
    @Query(value = "SELECT * FROM admin_user u WHERE u.username = :username and u.password = md5(:password)", nativeQuery = true)
    AdminUser findByUsernameAndPassword(@Param("username") String username,@Param("password") String password);
}
