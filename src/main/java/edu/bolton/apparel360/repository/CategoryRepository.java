/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.repository;

import edu.bolton.apparel360.model.Category;
import edu.bolton.apparel360.model.SubCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sachin
 */
@Repository
public interface CategoryRepository extends CrudRepository<Category, String>{
    /*@Query(
  value = "SELECT * FROM sub_category sc,category_sub_categories csc WHERE sc.id=csc.sub_categories_id and csc.sub_categories_id= :subcategoryid", 
  nativeQuery = true)
    SubCategory f(@Param("subcategoryid") String subcategoryid);
    SubCategory findBySubCategoriesId(String subCategoryiesId);*/
}
