package edu.bolton.apparel360.repository;

import edu.bolton.apparel360.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author thisu96
 * @Date 21/07/2020
 * @Time 13:51
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query(value = "SELECT * from payment where order_id = ?1",nativeQuery = true)
    Payment findByOrderId(long orderId);
}
