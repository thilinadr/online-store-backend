/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.util;

import java.text.NumberFormat;

/**
 *
 * @author Sachin
 */
public class Utility {
    public static String convertDouble(Double value){
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setGroupingUsed(false);
        nf.setMinimumFractionDigits(2);
        String amount= nf.format(value);
        
        return amount;
    }
}
