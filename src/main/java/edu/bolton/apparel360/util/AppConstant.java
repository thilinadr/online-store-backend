package edu.bolton.apparel360.util;

import java.util.Date;

public class AppConstant {
    public static final String NO_VALUE="";
    public static final String SPACE=" ";
    public static final String HIPEN="-";
    public static final String DEFAULT_DATE_STRING="1/1/1980";
    public static final Date NO_DATE=new Date();
    public static final Date NULL_DATE=null;
    public static final int ZERO=0;
    public static final String ADMIN_ROOT_URL = "/admin";
    public static final String ADMIN_LOGIN_URL = "/admin/login";
    public static final String ADMIN_LOGOUT_URL = "/admin/logout";
    public static final String LOGIN_URL="/login";
    public static final String LOGOUT_URL="/logout";
    public static final String ROOT_URL="/";  
}
