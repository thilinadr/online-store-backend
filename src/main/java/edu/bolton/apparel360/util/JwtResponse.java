/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bolton.apparel360.util;

import edu.bolton.apparel360.dto.CustomerDto;

import java.io.Serializable;

/**
 *
 * @author Sachin
 */
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private CustomerDto customer;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }
    public JwtResponse(String jwttoken,CustomerDto customer) {
        this.jwttoken = jwttoken;
        this.customer = customer;
    }

    public String getToken() {
        return this.jwttoken;
    }

    public CustomerDto getCustomer(){
        return this.customer;
    }
}
