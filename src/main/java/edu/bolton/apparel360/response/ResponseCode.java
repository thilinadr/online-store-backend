package edu.bolton.apparel360.response;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:09
 */
public final class ResponseCode {
    public static final Integer MISSING_DATA = 400;
    public static final Integer CONFLICT_DATA_VALUE = 405;
    public static final Integer SUCCESS = 200;
    public static final Integer DUPLICATE_DATA = 406;
}
