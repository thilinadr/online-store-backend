package edu.bolton.apparel360.response;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 11:22
 */
public class Messages {
    private Messages() {
        throw new IllegalStateException("Util Class");
    }
    public static final String CONCURRENT_ISSUE = "Expection Occured While Trying to Persist Data.";
}
