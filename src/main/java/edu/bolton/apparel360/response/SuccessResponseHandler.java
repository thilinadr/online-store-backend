package edu.bolton.apparel360.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author thisu96
 * @Date 12/07/2020
 * @Time 12:45
 */
public class SuccessResponseHandler {
    public static ResponseEntity<SuccessResponse> generateResponse(Object data) {
        return new ResponseEntity<>(new SuccessResponse(data), HttpStatus.OK);
    }

    public static ResponseEntity<SuccessResponse> generateResponse(Object data,String message) {
        return new ResponseEntity<>(new SuccessResponse(data,message), HttpStatus.OK);
    }
}
